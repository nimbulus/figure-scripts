reset
#!/usr/bin/gnuplot -persist

set datafile separator ","

fileout = "CV-with-and-without-RCs.ps"
data1hrMCH_10uMrcs = "csvs/csvs/szaaoa02_07.csv"
data1hrMCH_noRCs = "csvs/csvs/szaaoa04_03.csv"


#####################x and y labels
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitleDeltCuruA = "{/Symbol D} i_{ph} (nA)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"
set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.8"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 8
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

stats data1hrMCH_10uMrcs u 3:($4*1e6) nooutput
set format x "%0.2f"
set format y "%0.1f"
#set size @plot_size
set ylabel ytitleCuruA
set xlabel xtitleAgAgCl
set xrange [-0.12:0.3]
set yrange [STATS_min_y-20:STATS_max_y+40]
#set ytics 750/5
set key at 0.3,STATS_max_y+40 font ",14"
#set label 1 "805 nm" at -.1,STATS_max_y/2 + 10 font ", 30"

plot \
data1hrMCH_noRCs u 3:($4*1e6) axis x1y1 w l title "CV of 1hr MCH without RCs " ls 5, \
data1hrMCH_10uMrcs u 3:($4*1e6) axis x1y1 w l title "CV of 1hr MCH, coated with 10uM of DM RCs for 10 minutes" ls 2, \

