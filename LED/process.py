 
import pandas as pd
from scipy.signal import savgol_filter
from scipy import sparse
from scipy.sparse.linalg import spsolve
from matplotlib import pyplot as plt
import numpy as np
from statistics import mean, stdev 
from scipy.signal import find_peaks


df = pd.read_csv("leds.csv")

x = df['865 emission']
y865 = df['intensity']
y805 =df['intensity.1']
y680 = df['intensity.2']
plt.plot(x,y865,'-',label=r"Emission curve of 865nm LED")
plt.plot(x,y805,'-',label=r"Emission curve of 810nm LED")
plt.plot(x,y680,'-',label=r"Emission curve of 680nm LED")

#plt.xticks([])
#plt.yticks([])
#plt.xticks(np.arange(-1.24, 0.63, step=0.2))
#plt.yticks(np.arange(-1.1e-4, 9.5e-5, step=0.2))
#plt.plot(diE,diI,'.',label=r"Current vs Potential, for an electrode coated with RCs and MCH")
plt.xlabel(r'$\lambda$ (nm)')
plt.xlim([500,900])
plt.ylabel('Emission Response')
plt.legend()
plt.show()
