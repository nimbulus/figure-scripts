reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "deltaIvssqrtt.ps"	#does not use datapath

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
datapath = "dcmethods2/csv/"

datadeltaIvssqrtT = "dcmethods2/csv/nomch5um-2-sqrt25s.csv"
data_nomch5um2 = "nomch5um2.csv"

ttitle = "t (sec)"
xtitle = "E (V)"
xtitle1sqrtt = "1/sqrt(t)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"
ytitleDeltCuruA = "{/Symbol D} i_{ph} (nA)"
set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.38"

ymax_pc=400

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  805 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0

set xlabel xtitle1sqrtt
set format x "%0.1f"
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel "{/Symbol D} i_{ph} (nA)\n(relative to t=24.1)" font ",20"
set size @plot_size




stats datadeltaIvssqrtT u 1:($2*1e9) nooutput


set origin 0.15, .6

unset y2tics
set ytics mirror
set yrange [0:40]
set ytics 40/4
set xrange [0:2.5]
#unset key
set key left top font "Helvetica,12" box
set xzeroaxis

#set label 1 "Faradaic currents (a) and Photocurrent at 805nm (b) \nof DMs deposited for varying times" at -.15,ymax_fc+40 font ", 16"

set label "a)" at -0.22,(77) font ",24"

linefit(x) = a*x + b
fit linefit(x) datadeltaIvssqrtT every ::2::5000 u 1:($2*1e9 - STATS_min_y) via a, b
plot \
datadeltaIvssqrtT every ::2::5000 u 1:($2*1e9 - STATS_min_y) axis x1y1 w p title "5uM RCs 24hr, no MCH" ls 2 pointsize 0.7, \
#linefit(x) axis x1y1 w l title sprintf('linear fit: f(x) = %.2fx + %.2f', a, b) ls 2 pointsize 0.7, \

#datapath.datanoRCs u 1:($7*1e6) axis x1y1 w l smooth bezier title "no RCs" ls 5, \

unset label

unset yrange
unset xrange
###############COMPARE WITH t########################

set bmargin 0.05
set ylabel ytitleDeltCuruA offset 1,0

set xlabel ttitle offset 0,0.6
set format x "%0.1f"
set format y "%0.1f"
set size @plot_size
set origin 0.15, 0.125
unset y2tics
set ytics 20
set ytics mirror
#set yrange [ymin:ymax_pc]
#set key at 0.13,3.8
unset key
stats datapath.data_nomch5um2 u 3:($4*1e9) nooutput

set label 1 "b)" at 6,85 + 10 font ",24"
##auto-picks the extremae of

set xrange [23:35]
set yrange [0:80]
set key top left box font ",14"
#set xtics (STATS_max_x-STATS_min_x)*0.25
#light on and off rectangles
set style rect fc lt -1 fs solid 0.15 noborder
set object 1 rect from 24.1, graph 0 to 34.4, graph 1 back fc rgb "yellow"
#set object 2 rect from 14.5, graph 0 to 24, graph 1 back

unset object 3
unset object 4
unset object 5



plot \
datapath.data_nomch5um2 u 3:($4*1e9 - STATS_min_y) axis x1y1 w l title "5uM DMs for 24 hours and no MCH" ls 2, \

unset label 1
unset key
#cleanu

set bmargin 0.

unset multiplot

quit

