reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "baregold-vs-rcs-but-no-mch.ps"	#does not use datapath

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
###ups
datapath = "csvs/csvs/"

cvdatabaregold = "szaaa01_09.csv"
cvdataonlyRCs = "szabaj02_01.csv" #_805.csv"
cvdataRCsPC = "negative-to-positive/5um24hrnoMCH.csv_negative-to-positive_805-comma.csv"


###


ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  805 nm  #####################

set multiplot layout 2,1	#2 high, one wide
#set multiplot 2,2

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

stats datapath.cvdatabaregold u 4:($5*1e6) nooutput

set xrange [-0.2:STATS_max_x]
set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics STATS_max_y/5
#unset y2tics
set ytics mirror
set yrange [STATS_min_y:STATS_max_y]
set key at 0.27,STATS_max_y font ",16"
set xzeroaxis

set label 1 "CV of bare gold in 25uM HQ, and of an electrode \ncovered in 10uM RCs for 24 hours, in 20uM HQ" font ",16" at -.19,600

set label "a)" at -0.225,STATS_max_y+80 font ",20"

plot \
datapath.cvdatabaregold u 4:($5*1e6) axis x1y1 w l title "Bare gold (in 25uM HQ)" ls 1, \
datapath.cvdataonlyRCs u 5:($6*1e6) axis x1y1 w l title "Gold coated in 10uM RCs for 24 hours (20uM HQ)" ls 2, \

unset label

set bmargin 0.05
stats cvdataRCsPC u 1:($3*1e9) nooutput
set xrange [-0.08:0.27]

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.6
set ylabel ytitleCurnA
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 50/5
unset y2tics
set ytics mirror
set yrange [-20:50]
#set key at 0.13,3.8
unset key
set key at 0.27,50 font ",16"

set label 1 "b)" at -0.225,50 font ",24"
############################################################
#
#set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
#set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
#set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
#set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
#set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
#set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
#set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
#set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"

plot \
cvdataRCsPC u 1:($3*1e9 - STATS_min_y) axis x1y1 w l title "Gold coated in 10uM RCs for 24 hours (20uM HQ)" ls 2, \

unset label 1
unset key
#cleanu
unset multiplot

quit

