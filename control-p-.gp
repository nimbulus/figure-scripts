reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "control-P-.ps"	#does not use datapath
datapathu = "data_no_gaps/negative-to-positive/"
datapathd = "data_no_gaps/positive-to-negative/"

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
###ups
dataPno865 = "szabag02_04_1uM_DM_P-_865nm.csv_0th_scan_negative-to-positive_680.csv"
dataPno805 = "szabag02_05_1uM_DM_P-_805nm.csv_0th_scan_negative-to-positive_680.csv"
dataPyes865 = "szabaf01_04_10min_1uM_DM_RCs_1hr_MCH_865nm.csv_0th_scan_negative-to-positive_680.csv"
dataPyes805 = "szabaf01_06_1uM_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv"
###downs
ddataPno865 = "szabag02_04_1uM_DM_P-_865nm_ded.csv_0th_scan_positive-to-negative_680.csv"
ddataPno805 = "szabag02_05_1uM_DM_P-_805nm.csv_0th_scan_positive-to-negative_680.csv"
ddataPyes865 = "szabaf01_04_10min_1uM_DM_RCs_1hr_MCH_865nm.csv_0th_scan_positive-to-negative_680.csv"
ddataPyes805 = "szabaf01_06_1uM_RC_100uMMCH24hr.csv_0th_scan_positive-to-negative_680.csv"
###

ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"

ymax_pc=600
ymax_far=100
ymin_far=-50
########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  805 nm  #####################

set multiplot layout 2,1	#2 high, one wide
#set multiplot 2,2

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1
set xrange [-0.12:0.3]
set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics ymax_far/5
unset y2tics
set ytics mirror
set yrange [ymin_far:ymax_far]
set key left top font "Helvetica,9" #box title "100uM MCH coverage with and without RCs" font "Helvetica,12"
set xzeroaxis

#set label 1 "Faradaic currents (a) and photocurrents at 805nm (b)" at -.1,ymax_far+20 font ", 20"

set label "a)" at -0.19,ymax_far font ",24"
#
#set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
#set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
#set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
#set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
#set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
#set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
#set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
#set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"

#
plot \
datapathu.dataPno865 u 1:($7*1e6) axis x1y1 w l smooth bezier title "1uM DMs P- 10 min, 24hr MCH (iluminated at 865nm)" ls 1, \
datapathu.dataPno805 u 1:($7*1e6) axis x1y1 w l smooth bezier title "1uM DMs P- 10 min, 24hr MCH (iluminated at 805nm)" ls 2, \
datapathu.dataPyes865 u 1:($7*1e6) axis x1y1 w l smooth bezier title "1uM DMs P+ 10 min, 24hr MCH (iluminated at 865nm)" ls 3, \
datapathu.dataPyes805 u 1:($7*1e6) axis x1y1 w l smooth bezier title "1uM DMs P+ 10 min, 24hr MCH (iluminated at 805nm)" ls 4, \
datapathd.ddataPno865 u 1:($7*1e6) axis x1y1 w l smooth bezier lc rgb "red" dt 2 title "", \
datapathd.ddataPno805 u 1:($7*1e6) axis x1y1 w l smooth bezier lc rgb "blue" dt 2 title "", \
datapathd.ddataPyes865 u 1:($7*1e6) axis x1y1 w l smooth bezier lc rgb "green" dt 2 title "", \
datapathd.ddataPyes805 u 1:($7*1e6) axis x1y1 w l smooth bezier lc rgb "black" dt 2 title "", \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.6
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:ymax_pc]
#set key at 0.13,3.8
#unset key
#set key at 0.13,400 font ",16"
unset key

set label 1 "b)" at -0.19,ymax_pc font ",24"
############################################################
#
#set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
#set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
#set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
#set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
#set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
#set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
#set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
#set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"

plot \
datapathu.dataQAno u 1:($3*1e9) axis x1y1 w l smooth bezier title "100uM MCH for 1 hour, no RCs" ls 2, \
datapathu.dataQAyes6um u 1:($3*1e9) axis x1y1 w l smooth bezier title "100uM MCH for 1 hour, 10uM RCs 10 min (second)" ls 1, \
datapathd.ddataQAno u 1:($3*1e9) axis x1y1 w l smooth bezier lc rgb "blue" dt 2 title "" ,\
datapathd.ddataQAyes6um u 1:($3*1e9) axis x1y1 w l smooth bezier lc rgb "red" dt 2 title "" ,\

unset label 1
unset key
#cleanu
unset multiplot

quit

