reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "dms-langmuirs.ps"	#does not use datapath
datapathu = "data_no_gaps/negative-to-positive/"
datapathd = "data_no_gaps/positive-to-negative/"

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
data0_0uM = "szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_negative-to-positive_680.csv"
data0_5uM = "szabah01_04_0.5uM_RC_100uMMC24hr.csv_1th_scan_negative-to-positive_680.csv" #_805.csv"
data1_0uM =  "szabaf01_06_1uM_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv" #_805.csv"
data2_5uM = "szabah02_06_2.5uM_RC_100uMMCH24hr.csv_1th_scan_negative-to-positive_680.csv" #_805.csv"
data5_0uM =  "szabaf02_045uM_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv" #szabaf02_04
data5_0uM2 =  "szabai02_04_5uM(2)_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv" #_805.csv"
dataNoRCs="szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_negative-to-positive_680.csv" #done

ddata0_0uM = "szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_positive-to-negative_680.csv"
ddata0_5uM = "szabah01_04_0.5uM_RC_100uMMC24hr.csv_1th_scan_positive-to-negative_680.csv"
ddata1_0uM = "szabaf01_06_1uM_RC_100uMMCH24hr.csv_0th_scan_positive-to-negative_680.csv"
ddata2_5uM = "szabah02_06_2.5uM_RC_100uMMCH24hr.csv_1th_scan_positive-to-negative_680.csv"
ddata5_0uM = "szabaf02_045uM_RC_100uMMCH24hr.csv_0th_scan_positive-to-negative_680.csv"
ddata5_0uM2 = "szabai02_04_5uM(2)_RC_100uMMCH24hr.csv_0th_scan_positive-to-negative_680.csv"
ddataNoRCs = "szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_positive-to-negative_680.csv"
#data5_0uM24 =  "5uM_24hr_1hrMCH.csv_negative-to-positive" #_805.csv"
#data10uM = "10uMDms10min100uMMCH1hr.csv_negative-to-positive_805.csv"
#data10uMnoMCH = "10uMDMsnoMCH.csv_negative-to-positive_805.csv"
data_langmuir = "langmuir.csv"
#data_langmuir2 = "langmuir-0.2V.csv"
#data_langmuir15 = "langmuir-0.15V.csv"
data_fit_langmuir = "fitted-langmuir.csv"
#data_fit_langmuir2 = "fitted-langmuir0.2V.csv"
#data_fit_langmuir15 = "fitted-langmuir0.15V.csv"
dataDMPno = "DM P-.csv_negative-to-positive"  #done



#dataFCcontrol = "ferrocyanide-control.csv_negative-to-positive" #done
#dataNoLight="nolight_controls.csv_negative-to-positive" #this only exists and has no wavelength for obvious reasons. 
#data5_0uM24_noMCH = "5um24hrnoMCH.csv_negative-to-positive"
wv680 = "_680.csv"
wv805 = "_805.csv"
wv865 = "_865.csv"


#805cf = 2.636 #correction factor for the power of 805nm LED
#76.89 = 1 #ditto for 680nm LED



ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  805 nm  #####################
ymax_pc = 400
#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 30
unset y2tics
set ytics mirror
set yrange [-30:90]
set key left top font "Helvetica,10" box title "10 min RC, 100uM MCH 24 hr" font "Helvetica,10"
set xzeroaxis

#set label 1 "805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,90 font ",24"

plot \
datapathu.data0_5uM u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM RCs" ls 1, \
datapathu.data1_0uM u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM RCs" ls 2, \
datapathu.data2_5uM u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM RCs" ls 3, \
datapathu.data5_0uM u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM RCs " ls 4, \
datapathu.dataNoRCs u 1:($7*1e6) axis x1y1 w l smooth csplines title "0uM RCs " ls 5, \
datapathd.ddata0_5uM u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
datapathd.ddata1_0uM u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
datapathd.ddata2_5uM u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "green" dt 2 title "", \
datapathd.ddata5_0uM u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \
datapathd.ddataNoRCs u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \
#datapathu.data10uM u 1:($7*1e6) axis x1y1 w l smooth csplines title "10.0uM (1hr 100uM MCH)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.6
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:ymax_pc]
#set key at 0.13,3.8
unset key
#set key at 0.13,ymax_pc-20 font ",16"
#set key left top font "Helvetica,10" box title "10 min RC, 100uM MCH 24 hr" font "Helvetica,12"

set label 1 "b)" at -0.225,ymax_pc+10 font ",24"

#highlight where the dots are collected from 
set style rect fc lt -1 fs solid 0.15 noborder
set object 1 rect from 0.249, graph 0 to 0.251, graph 1 back


plot \
datapathu.data0_5uM u 1:($3*1e9*1) axis x1y1 w l smooth bezier title "0.5uM RCs" ls 1, \
datapathu.data1_0uM u 1:($3*1e9*1) axis x1y1 w l smooth bezier title "1.0uM RCs" ls 2, \
datapathu.data2_5uM u 1:($3*1e9*1) axis x1y1 w l smooth bezier title "2.5uM RCs" ls 3, \
datapathu.data5_0uM u 1:($3*1e9*1) axis x1y1 w l smooth bezier title "5.0uM RCs" ls 4, \
datapathu.dataNoRCs u 1:($3*1e9*1) axis x1y1 w l smooth bezier title "0uM RCs" ls 5, \
datapathd.ddata0_5uM u 1:($3*1e9*1) axis x1y1 w l smooth bezier lc rgb "red" dt 2 title "", \
datapathd.ddata1_0uM u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
datapathd.ddata2_5uM u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "green" dt 2 title "", \
datapathd.ddata5_0uM u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \
datapathd.ddataNoRCs u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \

#datapathu.data10uM u 1:($3*1e10*1) axis x1y1 w l title "10.0uM RCs (1hr 100uM MCH)" ls 6, \
#datapathu.data10uMnoMCH u  1:($3*1e8*1) axis x1y1 w l title "10.0uM RCs (no mch)" ls 7, \
#10uM is was interpreted as 680 by splitscan.py. 

################INSET PLOT
unset label
unset key
set key at 0,350 font ",10" title ""
set size 0.4,0.28
# set size of inset #plot size is 0.8,0.4
set ytics ymax_pc/4 font "Times-Roman,10" offset 0.4
set ylabel ytitleCurnA." at 0.25V" font "Times-Roman,10" offset 3
#unset ytics
#set ylabel ""
set xtics 1 font "Times-Roman,10" offset 0,0.5
set xlabel "[RC]s ({/Symbol m}M)" font "Times-Roman,10" offset 0,1.5
set yrange [0:400]
set xrange [0:5.5]
set origin 0.24, 0.21
# move bottom left corner of inset
plot \
datapathu.data_langmuir u 1:($2*1e9*1) axis x1y1 with points ls  2title "" ,\
datapathu.data_fit_langmuir u 1:($2*1e9*1) axis x1y1 with l title "MaxPC = 240nA, Keq=9.13e-01" ,\


# plot inset

#unset multiplot
######END PLOT INSET

unset label 1
unset key
#cleanu
unset multiplot

quit

