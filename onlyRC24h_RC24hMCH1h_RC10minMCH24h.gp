reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "onlyRC24h_RC24hMCH1h_RC10minMCH24h.ps"	#does not use datapath
datapath = "data_no_gaps/negative-to-positive/"
datapathd = "data_no_gaps/positive-to-negative/"

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
data24noMCH = "szabaj02_05_5uM_RCs_24hr_no_MCH.csv_0th_scan_negative-to-positive_680.csv"
#NOTE! THE POTENTIOSTAT WILL AUTOMATICALLY SWITCH TO 1mA AND THUS BOOST THE PC BY 10X!
data241hrMCH = "szabaj02_105uM_RCs_24hr_1hr_MCH.csv_0th_scan_negative-to-positive_680.csv"
data10minRC100uMMCH24hr = "szabaf02_045uM_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv"

ddata24noMCH = "szabaj02_05_5uM_RCs_24hr_no_MCH.csv_0th_scan_positive-to-negative_680.csv"
ddata241hrMCH = "szabaj02_105uM_RCs_24hr_1hr_MCH.csv_0th_scan_positive-to-negative_680.csv"
ddata10minRC100uMMCH24hr = "szabaf02_045uM_RC_100uMMCH24hr.csv_2th_scan_positive-to-negative_680.csv"
#dataNoLight="nolight_controls.csv_negative-to-positive" #this only exists and has no wavelength for obvious reasons. 
#data5_0uM24_noMCH = "5um24hrnoMCH.csv_negative-to-positive"
#wv680 = "_680.csv"
#wv805 = "_805.csv"
#wv865 = "_865.csv"


#805cf = 2.636 #correction factor for the power of 805nm LED
#76.89 = 1 #ditto for 680nm LED



ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"
###################YMAX
ymax_pc=400
ymax_far=200
ymin_far=-120
########################

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  805 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
set xrange [-0.12:0.3]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA offset 0.9,0
set size @plot_size

set origin 0.15, .55
set ytics ymax_far/5
unset y2tics
set ytics mirror
set yrange [ymin_far:ymax_far]
set key left top font "Helvetica,10" box title "5uM RCs" font "Helvetica,12"
set xzeroaxis

#set label 1 "Photocurrent (805 nm) and Faradaic current vs MCH deposition time " at -.12,ymax_far+ymax_far/8 font ", 15"

set label "a)" at -0.19,ymax_far font ",24"

plot \
datapath.data24noMCH u 1:($7*1e6*1) axis x1y1 w l smooth csplines title "RCs 24hrs, no MCH" ls 5, \
datapath.data241hrMCH u 1:($7*1e6*1) axis x1y1 w l smooth csplines title "RCs 24hrs, 1hr 100uM MCH" ls 1, \
datapath.data10minRC100uMMCH24hr u 1:($7*1e6*1) axis x1y1 w l smooth csplines title "RCs 10min, 24hr 100uM MCH" ls 2, \
datapathd.ddata24noMCH u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \
datapathd.ddata241hrMCH u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
datapathd.ddata10minRC100uMMCH24hr u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \


unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.5
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics ymax_pc/4
unset y2tics
set ytics mirror
set yrange [-25:ymax_pc]
#set key at 0.13,3.8
unset key
# set key left top font "Helvetica,10" 

set label 1 "b)" at -0.19,ymax_pc font ",24"


plot \
datapath.data24noMCH u 1:($3*1e8*1) axis x1y1 w l smooth csplines title  "" ls 5, \
datapath.data241hrMCH u 1:($3*1e8*1) axis x1y1 w l smooth csplines title "" ls 1, \
datapath.data10minRC100uMMCH24hr u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "" ls 2, \
datapathd.ddata24noMCH u 1:($3*1e8*1) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \
datapathd.ddata241hrMCH u 1:($3*1e8*1) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
datapathd.ddata10minRC100uMMCH24hr u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \

#1e8 for the datanoMCH because splitscan correctly got that it was 805nm, but in the lock-in settings, it was at 1000uA/10V as the faradaic current got quite high.
unset label 1
unset key
#cleanup

unset multiplot

quit

