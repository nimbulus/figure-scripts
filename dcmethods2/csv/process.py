#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 19:52:24 2020

@author: randon

Process the DC scans, by plotting the curves, and then removing the baseline, then 
plotting the curves again, then plotting them against sqrt(time), then producing 
an average DeltaI. 

"""
import pandas as pd
from scipy.signal import savgol_filter
from scipy import sparse
from scipy.sparse.linalg import spsolve
from matplotlib import pyplot as plt
import numpy as np
from statistics import mean, stdev 
from scipy.signal import find_peaks

#from numba import jit
#plt.rcParams.update({'font.size': 32})
import os
import csv
plt.rcParams.update({'font.size': 32})
plt.rcParams["figure.figsize"] = (16,12)

def average(ar):
    if len(ar)>0:
        return sum(ar)/len(ar)
    else:
        return -1
def smoother(y,filtersize=35):
    yhat = savgol_filter(y, filtersize, 5) 
    return yhat

#apparently unpublished??
#https://rdrr.io/cran/baseline/man/baseline.als.html
#p =  Weighting of positive residuals 
# lam= 2nd derivative constraint 
def baseline_als(y, lam, p, niter=10):
  L = len(y)
  D = sparse.diags([1,-2,1],[0,-1,-2], shape=(L,L-2))
  w = np.ones(L)
  for i in range(niter):
    W = sparse.spdiags(w, 0, L, L)
    Z = W + lam * D.dot(D.transpose())
    z = spsolve(Z, w*y)
    w = p * (y > z) + (1-p) * (y < z)
  return z

mypath = os.getcwd()
mypath = mypath
files = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
dfs = {}

for file in files:
    if ".csv" in file:
        print(file)
        b = pd.read_csv(file)
        dfs.update({file:b})
    else:
        pass
##Index(['Label:'#=time_stabilize, 'E', 'time', 'i'], dtype='object')
        
"""
Process the DC scans, by plotting the curves, and then removing the baseline, then 
plotting the curves again, then plotting them against sqrt(time), then producing 
an average DeltaI. 
"""

def plotCurves():
    for key in dfs:
        print(key)
        scan = dfs[key]
        OCP = scan['E'].min()
        OCP = str(OCP)
        time = scan['time']
        current = scan['i']
        plt.plot(time,current,label=r"Current (A) vs Time (S)")
        plt.xlabel(r'Time (S)')
        plt.ylabel('Current (A)')
        plt.legend()
        tit = key.replace(".csv","")
        tit = str(tit)
        tit = tit+", OCP: "+OCP+"V"
        #plt.title(tit,y=1.08,fontsize=17)
        #plt.savefig(tit+".png")
        plt.show()
    
def plotCurvesVsSqrtTime():
    for key in dfs:
        print(key)
        scan = dfs[key]
        OCP = scan['E'].min()
        OCP = str(OCP)
        time = scan['time']
        time = np.array(time)
        time = 1/np.sqrt(time)
        current = scan['i']
        plt.plot(time,current,'o',label=r"Current (A) vs 1/$\sqrt{\mathrm{Time (S)}}$")
        plt.xlabel(r'$\frac{1}{\sqrt{\mathrm{Time (S)}} }$')
        plt.ylabel('Current (A)')
        plt.legend()
        tit = key.replace(".csv","")
        tit = str(tit)
        tit = tit+", OCP: "+OCP+"V"
        #plt.title(tit,y=1.08,fontsize=17)
        #plt.savefig(tit+"_sqrt_time.png")
        plt.show()

def findStep(y):
    #https://stackoverflow.com/a/48001937
    dary = y
    dary -= np.average(dary)
    step = np.hstack((np.ones(len(dary)), -1*np.ones(len(dary))))
    dary_step = np.convolve(dary, step, mode='valid')
    start_step_indx,_ = find_peaks(dary_step)
    stop_step_indx,DS = find_peaks(-1*dary_step)
    #plt.plot(dary)
    #plt.plot(dary_step/10)
    return start_step_indx,stop_step_indx
def guessDeltaI():
    """Guesses the DeltaI by finding peaks and dips, and averaging the 
    previous 2 seconds, before them. Then it 
    """
    for key in dfs:
        scan = dfs[key]
        time = scan['time']
        current = scan['i']
        starts, stops = findStep(current)
        plt.plot(time,current)
        plt.plot(time[starts],np.ones_like(starts)*max(current),"o")
        plt.plot(time[stops],np.ones_like(stops)*max(current),"x")
        plt.show()
plotCurves()
plotCurvesVsSqrtTime()
guessDeltaI()