reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "dms-multilayer-hysteresis.ps"	#does not use datapath

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
###ups
data0_0uM = "szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_negative-to-positive_680.csv"
data0_5uM = "szabah01_04_0.5uM_RC_100uMMC24hr.csv_1th_scan_negative-to-positive_680.csv" #_805.csv"
data1_0uM =  "szabaf01_06_1uM_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv" #_805.csv"
ddata0_0uM = "szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_positive-to-negative_680.csv"
ddata0_5uM = "szabah01_04_0.5uM_RC_100uMMC24hr.csv_1th_scan_positive-to-negative_680.csv"
ddata1_0uM = "szabaf01_06_1uM_RC_100uMMCH24hr.csv_0th_scan_positive-to-negative_680.csv"
ddata5_0uM = "szabaf02_045uM_RC_100uMMCH24hr.csv_0th_scan_positive-to-negative_680.csv"
ddata5_0uM2 = "szabai02_04_5uM(2)_RC_100uMMCH24hr.csv_0th_scan_positive-to-negative_680.csv"


data5_0uM =  "szabaf02_045uM_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv" #_805.csv"
data5_0uM2 =  "szabai02_04_5uM(2)_RC_100uMMCH24hr.csv_0th_scan_negative-to-positive_680.csv" #_805.csv"
###These ones are relevant####
data2_5uM = "szabah02_06_2.5uM_RC_100uMMCH24hr.csv_1th_scan_negative-to-positive_680.csv" #_805.csv"
dataNoRCs= "szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_negative-to-positive_680.csv" #done
data5_0uM24 = "szabaj02_105uM_RCs_24hr_1hr_MCH.csv_0th_scan_negative-to-positive_680.csv"
data5_0uM24_noMCH = "szabaj02_05_5uM_RCs_24hr_no_MCH.csv_0th_scan_negative-to-positive_680.csv"

ddata2_5uM = "szabah02_06_2.5uM_RC_100uMMCH24hr.csv_1th_scan_positive-to-negative_680.csv"
ddataNoRCs = "szaaoa04_03_100uMMCHnoRCs.csv_1th_scan_positive-to-negative_680.csv"
ddata5_0uM24 = "szabaj02_105uM_RCs_24hr_1hr_MCH.csv_0th_scan_positive-to-negative_680.csv"
ddata5_0uM24_noMCH = "szabaj02_05_5uM_RCs_24hr_no_MCH.csv_0th_scan_positive-to-negative_680.csv"
####END RELEVENT ONES ###

wv680 = "_680.csv"
wv805 = "_805.csv"
wv865 = "_865.csv"


#805cf = 2.636 #correction factor for the power of 805nm LED
#76.89 = 1 #ditto for 680nm LED



datapathu = "data_no_gaps/negative-to-positive/"
datapathd = "data_no_gaps/positive-to-negative/"
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.21, 0.3"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"
#set key font ",15"
##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

###############BEGIN A,B ############################


#########################  805 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot 

set bmargin 0
set xrange [-0.12:0.31]
set xtics 0.15
set format x "%0.2f"
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA offset 1,0
set size @plot_size

set origin 0.12, .55
set ytics 60/3
unset y2tics
set ytics mirror
set yrange [-60:60]
set key at 0.3,50 font "Helvetica,8"
set xzeroaxis

#set label 1 "Faradaic currents a),c) and photocurrents\nat 805nm b),d) of multilayers" at -.05,280 font ", 18"

set label "a)" at -0.225,80 font ",24"
#
#set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
#set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
#set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
#set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
#set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
#set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
#set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
#set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"

#


plot \
datapathu.data2_5uM u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM RCs 10min\n24hr MCH " ls 2, \
datapathd.ddata2_5uM u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
#datapathu.data0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
#datapathu.data1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
#datapathu.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapathd.ddata0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb  "blue" dt 2 title "", \
#datapathd.ddata5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \

    
##################################################################################################################3
unset label

set bmargin 0.03

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.6
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.12, 0.125
set ytics 150
unset y2tics
set ytics mirror
set yrange [-25:800]
#set key at 0.13,3.8
#unset key
#set key at 0.13,400 font ",16"

set label 1 "b)" at -0.225,900 font ",24"

############################################################

plot \
datapathu.data2_5uM u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "" ls 2, \
datapathd.ddata2_5uM u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
#datapathd.ddata0_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata1_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
#datapathd.ddata5_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \
#datapathd.ddataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \
#datapathu.data0_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
#datapathu.data1_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
#datapathu.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapathu.dataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "No RCs" ls 5, \

unset label 1
#unset key
#cleanu
####################################################################
#########################BEGIN C,D##################################
#set multiplot layout 2,1	#2 high, one wide
#set xrange [-0.1:0.29]
#set xtics 0.15
set format x "%0.2f"
unset ylabel
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
unset xlabel
#set ylabel ytitleCuruA
set size @plot_size

set origin 0.43, .55
set ytics 150/3
unset y2tics
set ytics mirror
set yrange [-90:150]
set key at 0.3,120 font "Helvetica,8"
set xzeroaxis

set label "c)" at -0.225,190 font ",24"
#
#set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
#set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
#set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
#set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
#set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
#set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
#set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
#set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"

#


plot \
datapathu.data5_0uM24 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5uM RCs 24hr\n1hr MCH" ls 3, \
datapathd.ddata5_0uM24 u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "green" dt 2 title "", \
#datapathu.data0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
#datapathu.data1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
#datapathu.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapathd.ddata0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb  "blue" dt 2 title "", \
#datapathd.ddata5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \

    
##################################################################################################################3
unset label

set bmargin 0.03

#set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.6
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.43, 0.125
set ytics 150
unset y2tics
set ytics mirror
set yrange [-25:800]
set key at 0.3,700 font "Helvetica,8"

set label 1 "d)" at -0.225,900 font ",24"
############################################################

plot \
datapathu.data5_0uM24 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "" ls 3, \
datapathd.ddata5_0uM24 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "green" dt 2 title "", \
#datapathd.ddata0_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata1_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
#datapathd.ddata5_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \
#datapathd.ddataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \
#datapathu.data0_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
#datapathu.data1_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
#datapathu.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapathu.dataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "No RCs" ls 5, \

unset label 1
#unset key
#cleanu

################################BEGIN E,F############################
####################################################################
#faradaic current multilayer
unset label

set bmargin 0.03

unset ylabel
#set ylabel ytitleCurnA
#set xlabel xtitleAgAgCl
unset xlabel
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.74, 0.55
set ytics 150/3
unset y2tics
#unset ytics
#set ytics mirror
set yrange [-90:150]
#set xrange [-0.08:0.27]
set key at 0.3,120 font "Helvetica,8"
set label 1 "e)" at -0.225,190 font ",24"
############################################################

plot \
datapathu.data5_0uM24_noMCH u 1:($7*1e6*1) axis x1y1 w l smooth csplines title "5uM 24hr, no MCH" ls 1, \
datapathd.ddata5_0uM24_noMCH u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata0_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata1_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
#datapathd.ddata5_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \
#datapathd.ddataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \
#datapathu.data0_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
#datapathu.data1_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
#datapathu.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapathu.dataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "No RCs" ls 5, \

unset label 1
#unset key

####################################################################
unset label

set bmargin 0.03

#set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.6
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.74, 0.125
#set ytics 100
unset y2tics
#unset ytics
set ytics 150
#set ytics mirror
set yrange [-20:800]
#set key at 0.13,3.8
#set key at 0.29,750 font ",14"

set label 1 "f)" at -0.225,890 font ",24"
############################################################

plot \
datapathu.data5_0uM24_noMCH u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "" ls 1, \
datapathd.ddata5_0uM24_noMCH u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata0_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
#datapathd.ddata1_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
#datapathd.ddata5_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines lc rgb "black" dt 2 title "", \
#datapathd.ddataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines lc rgb "magenta" dt 2 title "", \
#datapathu.data0_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
#datapathu.data1_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
#datapathu.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapathu.dataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "No RCs" ls 5, \

unset label 1
unset key
#cleanu

#cleanu


unset multiplot

quit

