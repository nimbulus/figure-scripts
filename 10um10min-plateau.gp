 
reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "10um10min-plateau.ps"	#does not use datapath
datapathu = "data_no_gaps/negative-to-positive/"
datapathd = "data_no_gaps/positive-to-negative/"

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
###ups
#databaregold = "szaaa01_09"
#called 680 but actually 805. splitscan.py is responsible for this.
data10uMRCs  = "szaaoa02_07_10uMRCs_1hrMCH100uM.csv_1th_scan_negative-to-positive_680.csv"
data10uMRCs0 = "szaaoa02_07_10uMRCs_1hrMCH100uM.csv_0th_scan_negative-to-positive_680.csv"
data10uMRCsnoMCH = "szaaob02_03_10uMRCs_no_MCH.csv_1th_scan_negative-to-positive_680.csv"
data10uMRCsnoMCH0 = "szaaob02_03_10uMRCs_no_MCH.csv_0th_scan_negative-to-positive_680.csv"
###downs
ddata10uMRCs = "szaaoa02_07_10uMRCs_1hrMCH100uM.csv_1th_scan_positive-to-negative_680.csv"
ddata10uMRCs0 = "szaaoa02_07_10uMRCs_1hrMCH100uM.csv_0th_scan_positive-to-negative_680.csv"
ddata10uMRCsnoMCH= "szaaob02_03_10uMRCs_no_MCH.csv_1th_scan_positive-to-negative_680.csv"
ddata10uMRCsnoMCH2 = "szaaob02_03_10uMRCs_no_MCH.csv_2th_scan_positive-to-negative_680.csv"
###

wv680 = "_680.csv"
wv805 = "_805.csv"
wv865 = "_865.csv"


#805cf = 2.636 #correction factor for the power of 805nm LED
#76.89 = 1 #ditto for 680nm LED



datapathu = "data_no_gaps/negative-to-positive/"
datapathd = "data_no_gaps/positive-to-negative/"
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"
ymax_far=200
ymax_pc=800
########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  805 nm  #####################

set multiplot layout 2,1	#2 high, one wide
#set multiplot 2,2

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1
set xrange [-0.13:0.3]
set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics ymax_far/5
unset y2tics
set ytics mirror
set yrange [-ymax_far:ymax_far]
set key left top font "Helvetica,11" #box title "10uM RCs deposited for 10 minutes and with/without MCH" font "Helvetica,12"
set xzeroaxis

#set label 1 "", 30"

set label "a)" at -0.2,ymax_far font ",24"
#
#set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
#set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
#set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
#set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
#set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
#set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
#set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
#set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"

#
plot \
datapathu.data10uMRCs u 1:($7*1e6) axis x1y1 w l smooth csplines title "10uM RCs 10min, 1 hr 100uM MCH " ls 1, \
datapathu.data10uMRCsnoMCH u 1:($7*1e6) axis x1y1 w l smooth csplines title "10uM RCs 10 min, without MCH " ls 2, \
datapathd.ddata10uMRCs u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "red" dt 2 title "", \
datapathd.ddata10uMRCsnoMCH u 1:($7*1e6) axis x1y1 w l smooth csplines lc rgb "blue" dt 2 title "", \
#datapathu.data10uMRCs u 1:($7*1e6) axis x1y1 w l smooth csplines title "10uM RCs 10min, 1 hr 100uM MCH (second)" ls 2, \
#datapathu.data10uMRCsnoMCH u 1:($7*1e6) axis x1y1 w l smooth csplines title "10uM RCs 10 min, without MCH (second)" ls 4, \
#
#

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.6
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics ymax_pc/5
unset y2tics
set ytics mirror
set yrange [-25:ymax_pc]
#set key at 0.13,3.8
#unset key
#set key at 0.13,400 font ",16"

set label 1 "b)" at -0.2,ymax_pc font ",24"
############################################################
#
#set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
#set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
#set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
#set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
#set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
#set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
#set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
#set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"

plot \
datapathu.data10uMRCs u 1:($3*1e9) axis x1y1 w l smooth bezier title "10uM RCs 10 min, 1hr 100uM MCH " ls 1 , \
datapathu.data10uMRCsnoMCH u 1:($3*1e9) axis x1y1 w l smooth bezier title "10uM RCs 10 min, without MCH (10x)" ls 2, \
datapathu.data10uMRCsnoMCH u 1:($3*1e8) axis x1y1 w l smooth bezier title "10uM RCs 10 min, without MCH " dt 4, \
datapathd.ddata10uMRCs u 1:($3*1e9) axis x1y1 w l smooth bezier lc rgb "red" dt 2 title "",  \
datapathd.ddata10uMRCsnoMCH u 1:($3*1e9) axis x1y1 w l smooth bezier lc rgb "blue" dt 2 title "",  \
#1e10 because splitscan.py
unset label 1
unset key
#cleanu
unset multiplot

quit

