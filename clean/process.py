import pandas as pd
from scipy.signal import savgol_filter
from scipy import sparse
from scipy.sparse.linalg import spsolve
from matplotlib import pyplot as plt
import numpy as np
from statistics import mean, stdev 
from scipy.signal import find_peaks


df = pd.read_csv("cleanandunclean.csv")

clE = np.array(df['pot'])[:3000]
diE = np.array(df['pot.1'])[20:1500]
clI = np.array(df['I'])[:3000]
diI = np.array(df['I.1'])[20:1500]

plt.plot(clE,clI,'.',label=r"Current vs Potential, for a clean electrode")
#plt.xticks([])
#plt.yticks([])
#plt.xticks(np.arange(-1.24, 0.63, step=0.2))
#plt.yticks(np.arange(-1.1e-4, 9.5e-5, step=0.2))
plt.plot(diE,diI,'.',label=r"Current vs Potential, for an electrode coated with RCs and MCH")
plt.xlabel(r'Potential (V)')
plt.ylabel('Current (A)')
plt.legend()
plt.show()
