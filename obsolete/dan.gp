reset
#!/usr/bin/gnuplot -persist

fileout = "langmuirs.ps"	#does not use datapath
#Change me
#text files should have only 1 direction
dataRCnoHQoff = "0.5uM.csvup_680.csv"
dataRCnoHQon = "0.5uM.csvup_805.csv"
dataRCHQoff = "0.5uM.csvup_865.csv"
dataRCHQon = "Iph_Au10mMHQ_off_82.txt"
dataRCHQonoff = "Iph_Au10mMHQ_off_82.txt"
dataMCHHQon = "Iph_Au10mMHQ_off_82.txt"
dataMCHHQoff = "Iph_Au10mMHQ_off_82.txt"
dataAuHQon = "Iph_Au10mMHQ_off_82.txt"
dataAuHQoff = "Iph_Au10mMHQ_off_82.txt"
#
datapath = ""
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i (nA)"
ytitleCuruA = "i_{DC} ({/Symbol m}A)"
ytitleCurmA = "i_{DC} (mA)"
ytitleCurPh = "i_{PC} (nA)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"
ytitlePhase = "phase / deg"
set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "magenta"
set style line 5 lt 5 lw 2 pt 13  lc rgb "black"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 19 lt 1 lw 3 pt 13 dt 0.1 lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.275
set rmargin 0.075
#set size 0.8, 0.45


#set title ""
unset title
#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set size @plot_size

set origin 0.15, .55
set ytics 2
unset y2tics
set ytics mirror
set yrange [*:*]
set key at -0.275,10 left font ",16"
set xzeroaxis

set label "a)" at -0.45,10 font ",24"

#1:3:($3-$4):($3+$4)
#1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5)

plot \
datapath.dataRCnoHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "0mM HQ off" ls 1, \
datapath.dataRCnoHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "0mM HQ on" ls 3, \
datapath.dataRCHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ off" ls 5, \
datapath.dataRCHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ on" ls 7, \

unset label



set bmargin 0.05

set ylabel ytitleCuruA
set xlabel xtitleNHE
set format x "%0.1f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
set ytics mirror
#set yrange [0:3]
#set key at 0.05,3.8
unset key
set key at -0.275,100 left font ",16"

set label 1 "b)" at -0.45,100 font ",24"


plot \
datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "0mM HQ off" ls 1, \
datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "0mM HQ on" ls 3, \
datapath.dataRCHQoff u 1:($7/1.000) axis x1y1 w lp title "10mM HQ off" ls 5, \
datapath.dataRCHQon u 1:($7/1.000) axis x1y1 w lp title "10mM HQ on" ls 7, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot
############################################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set size @plot_size

set origin 0.15, .55
set ytics 2
unset y2tics
set ytics mirror
set yrange [*:*]
# set key horizontal outside  font ",16"
set xzeroaxis

set label "a)" at -0.45,10 font ",24"

plot \
datapath.dataRCnoHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "0mM HQ off" ls 1, \
datapath.dataRCnoHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "0mM HQ on" ls 3, \
datapath.dataRCHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ off" ls 5, \
datapath.dataRCHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ on" ls 7, \

unset label



set bmargin 0.05

set ylabel ytitlePhase
set xlabel xtitleNHE
set format x "%0.1f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
set ytics mirror
#set yrange [0:3]
#set key at 0.05,3.8
unset key
set key at 0.25,100 left font ",16"

set label 1 "b)" at -0.45,250 font ",24"

plot \
datapath.dataRCHQon u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y1 w yerrorbars title "RC 10mM HQ" ls 7, \

#:($7-$8):($7+$8)
unset label 1


#cleanup
unset multiplot
############################################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set format y2 "%0.0f"
unset y2label # ytitlePhase
plot_size = "0.7, 0.5"
set size @plot_size

set origin 0.15, .55
set ytics (0, 2, 4, 6, 8, 10)
set y2tics (-20, -10, 0, 10)
set ytics nomirror
set yrange [0:12.5]
set y2range [-60:10]
set key at -0.275,12.0 left font ",14"
set xzeroaxis

set label "a)" at -0.45,10 font ",24"
set label ytitlePhase at 0.75, 10 rotate by 90 center font "Times-Roman,20"

plot \
datapath.dataRCnoHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "0mM HQ off" ls 1, \
datapath.dataRCnoHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "0mM HQ on" ls 3, \
datapath.dataRCHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ off" ls 5, \
datapath.dataRCHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ on" ls 7, \
datapath.dataRCHQon every ::1::20 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 9, \

unset label



set bmargin 0.05


set ylabel ytitleCuruA
set xlabel xtitleNHE
unset y2label 
set format x "%0.1f"
set format y "%0.0f"
plot_size = "0.7, 0.4"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
set ytics mirror
set yrange [*:*]
#set key at 0.05,3.8
unset key
set key at -0.275,100 left font ",16"

set label 1 "b)" at -0.45,100 font ",24"


plot \
datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "0mM HQ off" ls 1, \
datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "0mM HQ on" ls 3, \
datapath.dataRCHQoff u 1:($7/1.000) axis x1y1 w lp title "10mM HQ off" ls 5, \
datapath.dataRCHQon u 1:($7/1.000) axis x1y1 w lp title "10mM HQ on" ls 7, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot

############################################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set format y2 "%0.0f"
unset y2label # ytitlePhase
plot_size = "0.7, 0.5"
set size @plot_size

set origin 0.15, .55
set ytics (0, 2, 4, 6, 8, 10)
set y2tics (-20, -10, 0, 10)
set ytics nomirror
set yrange [0:12.5]
set y2range [-60:10]
# set key horizontal outside  font ",16"
unset key
set key at -0.295,11 left font ",16"
set xzeroaxis

set label 1 "a)" at -0.45,10 font ",24"
set label 2 ytitlePhase at 0.75, 10 rotate by 90 center font "Times-Roman,20"

plot \
datapath.dataRCHQoff u 1:($3*2.5) axis x1y1 w l title "" ls 1, \
datapath.dataRCHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ off" ls 1, \
datapath.dataRCHQon u 1:($3*2.5) axis x1y1 w l  title "" ls 3, \
datapath.dataRCHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "10mM HQ on" ls 3, \
datapath.dataRCHQon every ::1::20 u 1:($5+80) axis x1y2 w l  title "" ls 9, \
datapath.dataRCHQon every ::1::20 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 9, \

#datapath.dataRCHQonoff u 1:(abs($2)) axis x1y1 w lp title "I_{DC} on-off" ls 5, \
unset label 1
unset label 2


set bmargin 0.05


set ylabel ytitleCuruA
set xlabel xtitleNHE
set y2label "{/Symbol D}i_{DC} ({/Symbol m}A)"
set format x "%0.1f"
set format y "%0.0f"
plot_size = "0.7, 0.4"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
set y2tics 5
#set ytics mirror
set yrange [-50:100]
set y2range [-5:10]
#set key at 0.05,3.8
unset key
set key at -0.275,100 left font ",16"

set label 1 "b)" at -0.45,100 font ",24"

# datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "off" ls 1, \
# datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "on" ls 3, \


plot \
datapath.dataRCHQoff u 1:($7/1.000) axis x1y1 w lp title "10mM HQ off" ls 1, \
datapath.dataRCHQon u 1:($7/1.000) axis x1y1 w lp title "10mM HQ on" ls 3, \
datapath.dataRCHQonoff u 1:($2) axis x1y2 w lp  title "{/Symbol D}i " ls 5, \
datapath.dataRCHQonoff u 1:($2*10) axis x1y2 w lp  title "{/Symbol D}i x10" ls 19, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot

############################################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set format y2 "%0.0f"
unset y2label # ytitlePhase
plot_size = "0.7, 0.5"
set size @plot_size

set origin 0.15, .55
set ytics (0, 2, 4) #, 4, 6, 8, 10)
set y2tics ( -90, 0, 90, 180)
set ytics nomirror
set yrange [0:12.5]
set y2range [-180:180]
# set key horizontal outside  font ",16"
set xzeroaxis
#/ZapfDingbats Q  /Symbol \052
set label 1 "a)" at -0.45,12 font ",24"
set label 2 ytitlePhase."({/Symbol \052})" at 0.75, 6.0 rotate by 90 center font "Times-Roman,20"

plot \
datapath.dataAuHQoff u 1:($3*2.5) axis x1y1 w l title "" ls 1, \
datapath.dataAuHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "Au 10mM HQ off" ls 1, \
datapath.dataAuHQon u 1:($3*2.5) axis x1y1 w l  title "" ls 3, \
datapath.dataAuHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "Au 10mM HQ on" ls 3, \
datapath.dataAuHQon every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 9, \
datapath.dataAuHQon every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 9, \

#datapath.dataRCHQonoff u 1:(abs($2)) axis x1y1 w lp title "I_{DC} on-off" ls 5, \
unset label 1
unset label 2


set bmargin 0.05


set ylabel ytitleCuruA
set xlabel xtitleNHE
unset y2label
# set y2label "{/Symbol D}i ({/Symbol m}A)"
set format x "%0.1f"
set format y "%0.0f"
plot_size = "0.7, 0.4"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
# set y2tics 5
#set ytics mirror
set yrange [-50:175]
# set y2range [-5:10]
#set key at 0.05,3.8
unset key
set key at -0.275,150 left font ",16"

set label 1 "b)" at -0.45,190 font ",24"

# datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "off" ls 1, \
# datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "on" ls 3, \


plot \
datapath.dataAuHQoff u 1:($7/1.000) axis x1y1 w lp title "Au 10mM HQ off" ls 1, \
datapath.dataAuHQon u 1:($7/1.000) axis x1y1 w lp title "Au 10mM HQ on" ls 3, \

# datapath.dataRCHQonoff u 1:($2) axis x1y2 w lp  title "{/Symbol D}i " ls 5, \
# datapath.dataRCHQonoff u 1:($2*10) axis x1y2 w lp  title "{/Symbol D}i x10" ls 19, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot

############################################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set format y2 "%0.0f"
unset y2label # ytitlePhase
plot_size = "0.7, 0.5"
set size @plot_size

set origin 0.15, .55
set ytics (0, 2, 4) #, 6, 8, 10)
set y2tics ( -90, 0, 90, 180)
set ytics nomirror
set yrange [0:12.5]
set y2range [-180:180]
# set key horizontal outside  font ",16"
set xzeroaxis

set label 1 "a)" at -0.45,12 font ",24"
set label 2 ytitlePhase."({/Symbol \052})" at 0.75, 6 rotate by 90 center font "Times-Roman,20"

plot \
datapath.dataMCHHQoff u 1:($3*2.5) axis x1y1 w l title "" ls 1, \
datapath.dataMCHHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "MCH 10mM HQ off" ls 1, \
datapath.dataMCHHQon u 1:($3*2.5) axis x1y1 w l  title "" ls 3, \
datapath.dataMCHHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "MCH 10mM HQ on" ls 3, \
datapath.dataMCHHQon every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 9, \
datapath.dataMCHHQon every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 9, \

#datapath.dataRCHQonoff u 1:(abs($2)) axis x1y1 w lp title "I_{DC} on-off" ls 5, \
unset label 1
unset label 2


set bmargin 0.05


set ylabel ytitleCuruA
set xlabel xtitleNHE
unset y2label
# set y2label "{/Symbol D}i ({/Symbol m}A)"
set format x "%0.1f"
set format y "%0.0f"
plot_size = "0.7, 0.4"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
# set y2tics 5
#set ytics mirror
set yrange [-50:100]
# set y2range [-5:10]
#set key at 0.05,3.8
unset key
set key at -0.275,100 left font ",16"

set label 1 "b)" at -0.45,110 font ",24"

# datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "off" ls 1, \
# datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "on" ls 3, \


plot \
datapath.dataMCHHQoff u 1:($7/1.000) axis x1y1 w lp title "MCH 10mM HQ off" ls 1, \
datapath.dataMCHHQon u 1:($7/1.000) axis x1y1 w lp title "MCH 10mM HQ on" ls 3, \

# datapath.dataRCHQonoff u 1:($2) axis x1y2 w lp  title "{/Symbol D}i " ls 5, \
# datapath.dataRCHQonoff u 1:($2*10) axis x1y2 w lp  title "{/Symbol D}i x10" ls 19, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot

############################################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set format y2 "%0.0f"
unset y2label # ytitlePhase
plot_size = "0.7, 0.5"
set size @plot_size

set origin 0.15, .55
set ytics (0, 2, 4) #, 6, 8, 10)
set y2tics ( -90, 0, 90, 180)
set ytics nomirror
set yrange [0:12.5]
set y2range [-180:180]
# set key horizontal outside  font ",16"
set xzeroaxis

set label 1 "a)" at -0.45,12 font ",24"
set label 2 ytitlePhase at 0.75, 6 rotate by 90 center font "Times-Roman,20"
set arrow 2 from 0.5,5.5 to 0.6,5.5 ls 3 filled front
set arrow 1 from 0.5,10 to 0.6,10 ls 1 filled front


plot \
datapath.dataRCHQoff u 1:($3*2.5) axis x1y1 w l title "" ls 1, \
datapath.dataRCHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "RC 10mM HQ off" ls 1, \
datapath.dataRCHQon u 1:($3*2.5) axis x1y1 w l  title "" ls 3, \
datapath.dataRCHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "RC 10mM HQ on" ls 3, \
datapath.dataRCHQon every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 3, \
datapath.dataRCHQon every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 3, \
datapath.dataRCHQoff every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 1, \
datapath.dataRCHQoff every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 1, \

#datapath.dataRCHQonoff u 1:(abs($2)) axis x1y1 w lp title "I_{DC} on-off" ls 5, \
unset label 1
unset label 2
unset arrow 1
unset arrow 2


set bmargin 0.05


set ylabel ytitleCuruA
set xlabel xtitleNHE
unset y2label
# set y2label "{/Symbol D}i ({/Symbol m}A)"
set format x "%0.1f"
set format y "%0.0f"
plot_size = "0.7, 0.4"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
# set y2tics 5
#set ytics mirror
set yrange [-50:100]
# set y2range [-5:10]
#set key at 0.05,3.8
unset key
set key at -0.275,100 left font ",16"

set label 1 "b)" at -0.45,110 font ",24"

# datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "off" ls 1, \
# datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "on" ls 3, \


plot \
datapath.dataRCHQoff u 1:($7/1.000) axis x1y1 w lp title "RC 10mM HQ off" ls 1, \
datapath.dataRCHQon u 1:($7/1.000) axis x1y1 w lp title "RC 10mM HQ on" ls 3, \

# datapath.dataRCHQonoff u 1:($2) axis x1y2 w lp  title "{/Symbol D}i " ls 5, \
# datapath.dataRCHQonoff u 1:($2*10) axis x1y2 w lp  title "{/Symbol D}i x10" ls 19, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot

############################################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set format y2 "%0.0f"
unset y2label # ytitlePhase
plot_size = "0.7, 0.5"
set size @plot_size

set origin 0.15, .55
set ytics (0, 2, 4) #, 6, 8, 10)
set y2tics (-90, 0, 90, 180)
set ytics nomirror
set yrange [0:12.5]
set y2range [-180:180]
# set key horizontal outside  font ",16"
set xzeroaxis

set label 1 "a)" at -0.45,12 font ",24"
set label 2 ytitlePhase at 0.75, 6 rotate by 90 center font "Times-Roman,20"
set arrow 2 from 0.5,10.5 to 0.6,10.5 ls 3 filled front
set arrow 1 from 0.5,7 to 0.6,7 ls 1 filled front

plot \
datapath.dataRCnoHQoff u 1:($3*2.5) axis x1y1 w l title "" ls 1, \
datapath.dataRCnoHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "RC 0mM HQ off" ls 1, \
datapath.dataRCnoHQon u 1:($3*2.5) axis x1y1 w l  title "" ls 3, \
datapath.dataRCnoHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "RC 0mM HQ on" ls 3, \
datapath.dataRCnoHQon every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 3, \
datapath.dataRCnoHQon every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 3, \
datapath.dataRCnoHQoff every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 1, \
datapath.dataRCnoHQoff every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 1, \

#datapath.dataRCHQonoff u 1:(abs($2)) axis x1y1 w lp title "I_{DC} on-off" ls 5, \
unset label 1
unset label 2
unset arrow 1
unset arrow 2


set bmargin 0.05


set ylabel ytitleCuruA
set xlabel xtitleNHE
unset y2label
# set y2label "{/Symbol D}i ({/Symbol m}A)"
set format x "%0.1f"
set format y "%0.0f"
plot_size = "0.7, 0.4"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
# set y2tics 5
#set ytics mirror
set yrange [-50:100]
# set y2range [-5:10]
#set key at 0.05,3.8
unset key
set key at -0.275,100 left font ",16"

set label 1 "b)" at -0.45,110 font ",24"

# datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "off" ls 1, \
# datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "on" ls 3, \


plot \
datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "RC 0mM HQ off" ls 1, \
datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "RC 0mM HQ on" ls 3, \

# datapath.dataRCHQonoff u 1:($2) axis x1y2 w lp  title "{/Symbol D}i " ls 5, \
# datapath.dataRCHQonoff u 1:($2*10) axis x1y2 w lp  title "{/Symbol D}i x10" ls 19, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot
#################PHASE###########################
set multiplot

set bmargin 0
set xrange [-0.3:0.625]
set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleNHE
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurPh
set format y2 "%0.0f"
unset y2label # ytitlePhase
plot_size = "0.7, 0.5"
set size @plot_size

set origin 0.15, .55
set ytics (0, 2, 4) #, 6, 8, 10)
set y2tics ( -90, 0, 90, 180)
set ytics nomirror
set yrange [0:12.5]
set y2range [-180:180]
# set key horizontal outside  font ",16"
set xzeroaxis

set label 1 "a)" at -0.45,12 font ",24"
set label 2 ytitlePhase at 0.75, 6 rotate by 90 center font "Times-Roman,20"
set arrow 2 from 0.5,5.5 to 0.6,5.5 ls 3 filled front
set arrow 1 from 0.5,10 to 0.6,10 ls 1 filled front


plot \
datapath.dataRCHQoff u 1:($3*2.5) axis x1y1 w l title "" ls 1, \
datapath.dataRCHQoff u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "RC 10mM HQ off" ls 1, \
datapath.dataRCHQon u 1:($3*2.5) axis x1y1 w l  title "" ls 3, \
datapath.dataRCHQon u 1:($3*2.5):($3*2.5-$4*2.5):($3*2.5+$4*2.5) axis x1y1 w yerrorbars title "RC 10mM HQ on" ls 3, \
datapath.dataRCHQon every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 3, \
datapath.dataRCHQon every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 3, \
datapath.dataRCHQoff every ::1::40 u 1:($5+80) axis x1y2 w l  title "" ls 1, \
datapath.dataRCHQoff every ::1::40 u 1:($5+80):($5-$6+80):($5+$6+80) axis x1y2 w yerrorbars title "phase" ls 1, \

#datapath.dataRCHQonoff u 1:(abs($2)) axis x1y1 w lp title "I_{DC} on-off" ls 5, \
unset label 1
unset label 2
unset arrow 1
unset arrow 2


set bmargin 0.05


set ylabel ytitleCuruA
set xlabel xtitleNHE
unset y2label
# set y2label "{/Symbol D}i ({/Symbol m}A)"
set format x "%0.1f"
set format y "%0.0f"
plot_size = "0.7, 0.4"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
# set y2tics 5
#set ytics mirror
set yrange [-50:100]
# set y2range [-5:10]
#set key at 0.05,3.8
unset key
set key at -0.275,100 left font ",16"

set label 1 "b)" at -0.45,110 font ",24"

# datapath.dataRCnoHQoff u 1:($7/1.000) axis x1y1 w lp title "off" ls 1, \
# datapath.dataRCnoHQon u 1:($7/1.000) axis x1y1 w lp title "on" ls 3, \


plot \
datapath.dataRCHQoff u 1:($7/1.000) axis x1y1 w lp title "RC 10mM HQ off" ls 1, \
datapath.dataRCHQon u 1:($7/1.000) axis x1y1 w lp title "RC 10mM HQ on" ls 3, \

# datapath.dataRCHQonoff u 1:($2) axis x1y2 w lp  title "{/Symbol D}i " ls 5, \
# datapath.dataRCHQonoff u 1:($2*10) axis x1y2 w lp  title "{/Symbol D}i x10" ls 19, \

#:($7-$8):($7+$8)
unset label 1
#cleanup
unset multiplot
##########END########
quit



