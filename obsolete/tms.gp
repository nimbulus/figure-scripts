reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "tms.ps"	#does not use datapath

# // columns arranged like:
#  ,Time (s),WE(1).Potential (V),WE(1).Current (A),Corrected time (s),Index,External(1).External 1 (V),External(1).External 2 (V)
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
dataTMexample = "5uM_TMcysplus_example_szaamb01_05_18_dec_500uV10V_100uA1V.csv" #_805.csv"
dataTMnocys =  "21uM_TMcysless_szajacys01_03_nov_11.csv" #_805.csv"
#data2_5uM = "2.5uM.csv_negative-to-positive" #_805.csv"
#data5_0uM =  "5uM.csv_negative-to-positive" #_805.csv"
#data5_0uM2 =  "5uM_2.csv_negative-to-positive" #_805.csv"
#data5_0uM24 =  "5uM_24hr_1hrMCH.csv_negative-to-positive" #_805.csv"
#dataDMPno = "DM P-.csv_negative-to-positive"  #done
#dataFCcontrol = "ferrocyanide-control.csv_negative-to-positive" #done
#dataNoRCs="no-rcs.csv_negative-to-positive" #done
#dataNoLight="nolight_controls.csv_negative-to-positive" #this only exists and has no wavelength for obvious reasons. 

#wv680 = "_680.csv"
#wv805 = "_805.csv"
#wv865 = "_865.csv"

datapath = "tms/"
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title
#########################  phase  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel xtitleAgAgCl
#set format x ""
# set xlabel xtitleAgAgCl
set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCurnA
set size @plot_size

set origin 0.15, .55
set ytics 40
unset y2tics
set ytics mirror
set yrange [-20:80]
set xrange [-0.1:0.3]
set key at 0.13,60 font ",16"
set xtics 0.1
set xzeroaxis

#set label 1 "" at -.1,60 font ", 30"

#set label "" at -0.225,50 font ",24"
#plot data using x:($y * scalefactor) axis which with points/lines title etc
#5e-9 Amps per Volt for the TMs
#5e-8 Amps per volt for Cys-
plot \
datapath.dataTMexample u 3:($7*5) axis x1y1 w l title "cys+" ls 1, \
datapath.dataTMnocys u 3:($7*5e1) axis x1y1 with line title "cys- (10x)" ls 2, \
#datapath.data2_5uM.wv680 u 1:($5/2) axis x1y1 with dot title "2.5uM" ls 3, \
#datapath.data5_0uM.wv680 u 1:($5/2) axis x1y1 with dot title "5.0uM" ls 4, \

#unset label
#unset label 1
unset key
#########################  phase  #####################



unset multiplot

quit
