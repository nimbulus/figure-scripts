reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator "	"


fileout = "langmuirs.ps"	#does not use datapath

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680

data0_5uM = "0.5uM.csv_negative-to-positive" #_805.csv"
data1_0uM =  "1uM.csv_negative-to-positive" #_805.csv"
data2_5uM = "2.5uM.csv_negative-to-positive" #_805.csv"
data5_0uM =  "5uM.csv_negative-to-positive" #_805.csv"
data5_0uM2 =  "5uM_2.csv_negative-to-positive" #_805.csv"
data5_0uM24 =  "5uM_24hr_1hrMCH.csv_negative-to-positive" #_805.csv"
dataDMP- = "DM P-.csv_negative-to-positive" 
dataFCcontrol = "ferrocyanide-control.csv_negative-to-positive" 
dataNoRCs="no-rcs.csv_negative-to-positive"
dataNoLight="nolight_controls.csv_negative-to-positive" #this only exists and has no wavelength for obvious reasons. 

wv680 = "_680.csv"
wv805 = "_805.csv"
wv865 = "_865.csv"

datapath = "negative-to-positive/"
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  680 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.1,40 font ",16"
set xzeroaxis

set label 1 "680 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:400]
#set key at 0.05,3.8
unset key
set key at 0.0,400 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \


unset label 1
unset key
#cleanup
unset multiplot

#########################  805 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:400]
#set key at 0.05,3.8
unset key
set key at 0.0,400 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label 1
unset key
#cleanup

unset multiplot

#########################  865 nm  #####################

set multiplot

set bmargin 0

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "865 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:400]
#set key at 0.05,3.8
unset key
set key at 0.0,400 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \


unset label 1

#cleanup


unset multiplot

########## comparing 5uM data 
#########################  680 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "680 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv680 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 2 (/5)" ls 5, \
datapath.data5_0uM24.wv680 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 24hr (/5)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-10:100]
#set key at 0.05,3.8
unset key
set key at 0.0,100 font ",16"

set label 1 "b)" at -0.225,100 font ",24"


plot \
datapath.data0_5uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 2" ls 5, \
datapath.data5_0uM24.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 24hr" ls 6, \


unset label 1
unset key
#cleanup
unset multiplot

#########################  805 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv805 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 2 (/5)" ls 5, \
datapath.data5_0uM24.wv805 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 24hr (/5)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:800]
#set key at 0.05,3.8
unset key
set key at 0.0,800 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 2" ls 5, \
datapath.data5_0uM24.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 24hr" ls 6, \

unset label 1
unset key
#cleanup

unset multiplot

#########################  865 nm  #####################

set multiplot

set bmargin 0

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "865 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv865 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 2 (/5)" ls 5, \
datapath.data5_0uM24.wv865 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 24hr (/5)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:800]
#set key at 0.05,3.8
unset key
set key at 0.0,800 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 2" ls 5, \
datapath.data5_0uM24.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 24hr" ls 6, \


unset label 1
###########################################################
#########################  Controls   #####################
###########################################################
set multiplot

set bmargin 0

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "865 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv865 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 2 (/5)" ls 5, \
datapath.data5_0uM24.wv865 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 24hr (/5)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:800]
#set key at 0.05,3.8
unset key
set key at 0.0,800 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 2" ls 5, \
datapath.data5_0uM24.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 24hr" ls 6, \


unset label 1





#cleanup


unset multiplot

quit

