#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 17:32:53 2020

@author: randon
"""
import pandas as pd
from scipy.signal import savgol_filter
from matplotlib import pyplot as plt
import numpy as np
from statistics import mean, stdev 
#from numba import jit
#plt.rcParams.update({'font.size': 32})
import os
import csv
#plt.rcParams["figure.figsize"] = (16,12)
#path = "/home/randon/Documents/homework/2019/bizzott/DMs Langmuirian/"

#@jit 

def average(ar):
    if len(ar)>0:
        return sum(ar)/len(ar)
    else:
        return -1
def extractScan(column,direction,boxsize=3,slope=0.01):
    retscan = []
        #returns indices of the scan going in a direction
        #later, one would then do pot = [scan["WE(1).Potential (V)"][i] for i in indices]
    column = np.array(column)
    if direction=="up":
        for i in range(len(column)-boxsize):
            if i>boxsize:
                #print(column[i-boxsize:i+boxsize])
                if mean(column[i-boxsize:i+boxsize])< mean(column[i-boxsize+1:i+boxsize+1]):
                    if abs(mean(column[i-boxsize:i+boxsize])-mean(column[i-boxsize+1:i+boxsize+1]) )<slope:
                #print(i)
                        retscan.append(i)
    else:
        for i in range(len(column)-boxsize):
            if i>boxsize:
                if mean(column[i-boxsize:i+boxsize])>mean(column[i-boxsize+1:i+boxsize+1]):
                    if abs(mean(column[i-boxsize:i+boxsize])-mean(column[i-boxsize+1:i+boxsize+1]) )<slope:
                #print(i)
                        retscan.append(i)
    return retscan

def findFullScans(column,direction,lower,upper,tolerance):
    retscan = []
    #I expect a np array as input
    print(direction)
    if direction=="up":
        #find the first lower node
        #since the full scan will contain both the highest and the lowest
        #potentials, its enough to find the max and the min of the collumn 
        #and collect everything in between (direction dependent)
        mins = []
        maxes = []
        realmin = []
        realmax = []
        for idx in range(len(column)):
            if abs(column[idx]-lower)<tolerance:
                #print("min is",min(column))
                #possibly found a continous stretch...
                mins.append(idx)
            if abs(column[idx] - upper) < tolerance:
                #print("max is",max(column))
                maxes.append(idx)
        
        #mush the maxes and mins to one
        #print('min',mins)
        #print('max',maxes)
        boxy = []
        for sus in range(len(mins)):
            #print("NO U")
            try:
                if mins[sus+1] - mins[sus] <200: #since we should expect a cluster but
                    #it wouldnt go to the next one
                    boxy.append(mins[sus])
                else:
                    #print('min boxy right now is',boxy)
                    realmin.append(int(np.floor(average(boxy))))
                    boxy = []
            except IndexError:
                #print('min boxy right now is',boxy)
                realmin.append(int(np.floor(average(boxy))))
                boxy = []
                #print("done")
        boxy = []
        for sus in range(len(maxes)):
            #print("NIGNOG")
            try:
                if abs(maxes[sus]-maxes[sus+1]) <200 : #since we should expect a cluster but
                    #it wouldnt go to the next one
                    #print(maxes[sus])
                    boxy.append(maxes[sus])
                else:
                    #print('max boxy right now is',boxy)
                    realmax.append(int(np.floor(average(boxy))))
                    boxy = []
            except IndexError:
                #print('max boxy right now is',boxy)
                realmax.append(int(np.floor(average(boxy))))
                boxy = []
                #print("doner done")
        #check that everything inside the stretches belong to the same scan
        #print(len(realmin))
        #print(len(realmax))
        #print(realmin,'realmin')
        #print(realmax,'realmax')
        mins = realmin
        maxes = realmax
        for i in range(len(mins)): #since only mins really count...
            try:
                if mins[i] < maxes[i]: #since we expect a single to be consecutive
                    maxl=maxes[i]
                else:
                    maxl = maxes[i+1]
                print(mins[i],"is lower node and",maxl,"is upper node")
                testindexes = np.linspace(mins[i],maxl,maxl-mins[i]).astype(int)
                #isgood = True
                retscan.append(testindexes)
            except IndexError:
                #retscan.append(testindexes)
                print("uh oh")
    if direction=="down":
        #find the first upper node
        #since the full scan will contain both the highest and the lowest
        #potentials, its enough to find the max and the min of the collumn 
        #and collect everything in between (direction dependent)
        #find the first lower node
        #since the full scan will contain both the highest and the lowest
        #potentials, its enough to find the max and the min of the collumn 
        #and collect everything in between (direction dependent)
        mins = []
        maxes = []
        realmin = []
        realmax = []
        for idx in range(len(column)):
            if abs(column[idx]-lower)<tolerance:
                #print("min is",min(column))
                #possibly found a continous stretch...
                mins.append(idx)
            if abs(column[idx] - upper) < tolerance:
                #print("max is",max(column))
                maxes.append(idx)
        
        #mush the maxes and mins to one
        #print('min',mins)
        #print('max',maxes)
        boxy = []
        for sus in range(len(mins)):
            #print("NO U")
            try:
                if mins[sus+1] - mins[sus] <200: #since we should expect a cluster but
                    #it wouldnt go to the next one
                    boxy.append(mins[sus])
                else:
                    #print('min boxy right now is',boxy)
                    realmin.append(int(np.floor(average(boxy))))
                    boxy = []
            except IndexError:
                #print('min boxy right now is',boxy)
                realmin.append(int(np.floor(average(boxy))))
                boxy = []
                #print("done")
        boxy = []
        for sus in range(len(maxes)):
            #print("NIGNOG")
            try:
                if abs(maxes[sus]-maxes[sus+1]) <200 : #since we should expect a cluster but
                    #it wouldnt go to the next one
                    #print(maxes[sus])
                    boxy.append(maxes[sus])
                else:
                   # print('max boxy right now is',boxy)
                    realmax.append(int(np.floor(average(boxy))))
                    boxy = []
            except IndexError:
               # print('max boxy right now is',boxy)
                realmax.append(int(np.floor(average(boxy))))
                boxy = []
               # print("done done")
        #check that everything inside the stretches belong to the same scan
        #print(len(realmin))
        #print(len(realmax))
        #print(realmin,'realmin')
        #print(realmax,'realmax')
        mins = realmin
        maxes = realmax
        for i in range(len(mins)): #since only mins really count...
            try:
                if mins[i] > maxes[i]: #since we expect a single to be consecutive
                    minl = mins[i]
                else:
                    minl = mins[i+1]
                print(minl,"is lower node and",maxes[i],"is upper node")
                testindexes = np.linspace(maxes[i],minl,abs(minl-maxes[i])).astype(int)
                retscan.append(testindexes)
                
            except IndexError:
                #retscan.append(testindexes)
                print("uh oh")    
    return retscan
def smoother(y,filtersize=51):
    yhat = savgol_filter(y, filtersize, 3) 
    return yhat
def rolling_window(a, window):
    #https://rigtorp.se/2011/01/01/rolling-statistics-numpy.html
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
def stdevs(y,boxsize=51): #returns the stdev within a size 51 box of y
    return np.std(rolling_window(y, boxsize), 1)
    
def smooth_box(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

mypath = os.getcwd()
files = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
dfs = {}

for file in files:
    if ".csv" in file:
        print(file)
        b = pd.read_csv(file)
        dfs.update({file:b})
    else:
        pass

def process(convfactor,wavkey,wavlen,direction):
    uponly = {}
    for sy in dfs:
        sys = dfs[sy]
        #export files should have the titles
        #Potential	stdev	Photocurrent (nA)	stdev	Phase (°)	stdev	DC Current (µA)	stdev
        try:
            print("i am",sy)
            indices = findFullScans(sys["WE(1).Potential (V)"+wavkey],direction,-0.06,0.295,0.001)
            print(indices)
            indices = indices[0] #being lazy, only doing the first 
            #print(indices)
            potentials = np.array([sys["WE(1).Potential (V)"+wavkey][i] for i in indices])
            potstdev = stdevs(potentials)        
            photocurrents = np.array([sys["External(1).External 1 (V)"+wavkey][i] for i in indices])*convfactor
            photocurrents_sm = smoother(photocurrents)
            pc_stdev = stdevs(photocurrents)
            phases = np.array([sys["External(1).External 2 (V)"+wavkey][i] for i in indices])*360/10
            ph_stdev = stdevs(phases)
            dcCurr = np.array([sys["WE(1).Current (A)"+wavkey][i] for i in indices])
            dcCurr_stdev = stdevs(dcCurr)
            
            
            uponly.update({sy:{'Potential':potentials,'potstdev':potstdev,'Photocurrent (nA)':photocurrents_sm, 'pcstdev':pc_stdev,"Phase (°)":phases,"phstdev":ph_stdev,"DC Current (µA)":dcCurr,"dcCurrstdev":dcCurr_stdev} })
        #except TypeError:
        #    print("wrong type of file",sy)
        except KeyError:
            print("missing key for",sy)

    for item in uponly:
        my_dict = uponly[item]
        fieldn = my_dict.keys()
        if direction=="up":
            ds = "_negative-to-positive"
        else:
            ds = "_positive-to-negative"
        csv_file = item+ds+"_"+wavlen+'.csv'
        for key in my_dict.keys():
            print(len(my_dict[key]))
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile,fieldnames=fieldn)
            writer.writeheader()
            #Detect the bottom of a full scan"
            for v in range(len(my_dict['potstdev'] ) -1):
                try:
                    #print(v)
                    tempdict = {}
                    for key in my_dict.keys():
                        #print(key)
                        tempdict.update({key:list(my_dict[key])[v] } )
                    writer.writerow(tempdict)
                except IndexError:
                    print(v)
                    print(key)
                    print(len(my_dict[key]))
                    print(1/0)

 # for 680 only, 5e-7 for others

for i in ["",".1",".2"]:
    if i == "":
        convfactor = 5e-8
        wl = "680"
    if i == ".1":
        wl = "805"
        convfactor = 5e-7
    if i ==".2":
        wl = "865"
        convfactor = 5e-7
    process(convfactor,i,wl,"up")
    process(convfactor,i,wl,"down")
