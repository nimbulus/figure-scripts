reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator "	"


fileout = "langmuirs.ps"	#does not use datapath

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
data0_0uM = "no-rcs.csv_negative-to-positive"
data0_5uM = "0.5uM.csv_negative-to-positive" #_805.csv"
data1_0uM =  "1uM.csv_negative-to-positive" #_805.csv"
data2_5uM = "2.5uM.csv_negative-to-positive" #_805.csv"
data5_0uM =  "5uM.csv_negative-to-positive" #_805.csv"
data5_0uM2 =  "5uM_2.csv_negative-to-positive" #_805.csv"
data5_0uM24 =  "5uM_24hr_1hrMCH.csv_negative-to-positive" #_805.csv"
dataDMPno = "DM P-.csv_negative-to-positive"  #done
dataFCcontrol = "ferrocyanide-control.csv_negative-to-positive" #done
dataNoRCs="no-rcs.csv_negative-to-positive" #done
dataNoLight="nolight_controls.csv_negative-to-positive" #this only exists and has no wavelength for obvious reasons. 
data5_0uM24_noMCH = "5um24hrnoMCH.csv_negative-to-positive"
wv680 = "_680.csv"
wv805 = "_805.csv"
wv865 = "_865.csv"


#805cf = 2.636 #correction factor for the power of 805nm LED
#76.89 = 1 #ditto for 680nm LED



datapath = "negative-to-positive/"
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.4"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  680 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.13,40 font ",16"
set xzeroaxis

set label 1 "680 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl offset 0,0.18
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 20
unset y2tics
set ytics mirror
set yrange [-25:100]
#set key at 0.13,3.8
unset key
#set key at 0.13,400 font ",16"

set label 1 "b)" at -0.225,100 font ",24"


plot \
datapath.data0_5uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \


unset label 1
unset key
#cleanup
unset multiplot

#########################  805 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:400]
#set key at 0.13,3.8
unset key
set key at 0.13,400 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label 1
unset key
#cleanup

unset multiplot

#########################  865 nm  #####################

set multiplot

set bmargin 0

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at -0.0,50 font ",16"
set xzeroaxis

set label 1 "865 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:400]
#set key at 0.13,3.8
unset key
set key at 0.13,400 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \


unset label 1

#cleanup


unset multiplot

########## comparing 5uM data 
#########################  680 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.13,50 font ",16"
set xzeroaxis

set label 1 "680 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv680 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 2 (/5)" ls 5, \
datapath.data5_0uM24.wv680 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 24hr (/5)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 20
unset y2tics
set ytics mirror
set yrange [-10:120]
#set key at 0.13,3.8
unset key
set key at 0.13,120 font ",16"

set label 1 "b)" at -0.225,120 font ",24"


plot \
datapath.data0_5uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM 2" ls 5, \
#datapath.data5_0uM24.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM 24hr" ls 6, \


unset label 1
unset key
#cleanup
unset multiplot

#########################  805 nm  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.131,50 font ",16"
set xzeroaxis

set label 1 "805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.0uM" ls 7, \
datapath.data0_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM    " ls 1, \
datapath.data1_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapath.data5_0uM2.wv805 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 2 (/5)" ls 5, \
#datapath.data5_0uM24.wv805 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 24hr (/5)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:400]
#set key at 0.13,50 font ",16"
#unset key
set key at 0.131,400 font ",16"

set label 1 "b)" at -0.225,400 font ",24"


plot \
datapath.data0_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.0uM" ls 7, \
datapath.data0_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
#datapath.data5_0uM2.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM 2" ls 5, \
#datapath.data5_0uM24.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM 24hr" ls 6, \

unset label 1
unset key
#cleanup

unset multiplot

#########################  865 nm  #####################

set multiplot

set bmargin 0

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.131,50 font ",16"
set xzeroaxis

set label 1 "865 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.data0_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv865 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 2 (/5)" ls 5, \
datapath.data5_0uM24.wv865 u 1:($7*1e6/5) axis x1y1 w l smooth csplines title "5.0uM 24hr (/5)" ls 6, \

unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:800]
#set key at 0.13,50 font ",16"
#unset key
set key at 0.131,800 font ",16"

set label 1 "b)" at -0.225,800 font ",24"


plot \
datapath.data0_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 4, \
datapath.data5_0uM2.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 2" ls 5, \
datapath.data5_0uM24.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM 24hr" ls 6, \


unset label 1
###########################################################
#########################  Controls   #####################
###########################################################

#######################865nm###############################
set multiplot

set bmargin 0

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.13,50 font ",16"
set xzeroaxis

set label 1 "DM P- Control 865 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"

plot \
datapath.dataDMPno.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "DM P- " ls 1, \
datapath.data5_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \
##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:300]
set key at 0.13,300
#unset key
#set key at 0.13,800 font ",16"

set label 1 "b)" at -0.225,300 font ",24"


plot \
datapath.dataDMPno.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "DM P-" ls 1, \
datapath.data5_0uM.wv865 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \

unset label 1

#######################805nm###############################
set multiplot

set bmargin 0
#bottom margin??

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.13,50 font ",16"
#key is the legend position. 
set xzeroaxis

set label 1 "DM P- control 805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"
#how this works

#plot <what to plot> <u =?> 1:<scale> axis <which axes> <w=?> <l=?> smooth = smoother csplines=style title "what is it labeled" ls=? 1=:color?
plot \
datapath.dataDMPno.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "DM P-" ls 1, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \
##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:300]
set key at 0.13,300 font ",16"
unset key
#set key at 0.13,500 font ",16"

set label 1 "b)" at -0.225,300 font ",24" 
#the positioning system is literally garbage...


plot \
datapath.dataDMPno.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "DM P-" ls 1, \
datapath.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \

unset label 1

#######################680nm###############################
set multiplot

set bmargin 0
#bottom margin??

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.13,50 font ",16"
#key is the legend position. 
set xzeroaxis

set label 1 "DM P- control 680 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"
#how this works

#plot <what to plot> <u =?> 1:<scale> axis <which axes> <w=?> <l=?> smooth = smoother csplines=style title "what is it labeled" ls=? 1=:color?
plot \
datapath.dataDMPno.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "DM P-" ls 1, \
datapath.data5_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \
##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 20
unset y2tics
set ytics mirror
set yrange [-25:100]
set key at 0.13,100 font ",16"
#unset key
##set key at 0.13,500 font ",16"

set label 1 "b)" at -0.225,100 font ",24"


plot \
datapath.dataDMPno.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "DM P-" ls 1, \
datapath.data5_0uM.wv680 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \

unset label 1


############################RC ferrocynanide control###########################



#######################805nm###############################
set multiplot

set bmargin 0
#bottom margin??

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
#unset y2tics
set ytics mirror
set yrange [-20:50]
#set y2range [-20:500]
#set y2tics 100
set key at 0.2,50 font ",16"
#key is the legend position. 
set xzeroaxis

set label 1 "Ferrocyanide control 805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"
#how this works
###faradaic current
#plot <what to plot> <u =?> 1:<scale> axis <which axes> <w=?> <l=?> smooth = smoother csplines=style title "what is it labeled" ls=? 1=:color?
plot \
datapath.dataFCcontrol.wv805 u 1:($7*1e6/10) axis x1y1 w l smooth csplines title "5.0 uM DMs in Ferrocyanide (/10)" ls 1, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM uM DMs in UQ" ls 2, \
##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
set ytics mirror
set yrange [-25:300]
set key at 0.13,300 font ",16"
unset key
#set key at 0.13,400 font ",16"

set label 1 "b)" at -0.225,300 font ",24" 
#the positioning system is literally garbage...
###photocurrent

plot \
datapath.dataFCcontrol.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "Ferrocyanide" ls 1, \
datapath.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 2, \

unset label 1

################################# NO RCS #######################
#######################805nm###############################
set multiplot

set bmargin 0
#bottom margin??

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.13,50 font ",16"
#key is the legend position. 
set xzeroaxis

set label 1 "MCH only control 805 nm" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"
#how this works

#plot <what to plot> <u =?> 1:<scale> axis <which axes> <w=?> <l=?> smooth = smoother csplines=style title "what is it labeled" ls=? 1=:color?
plot \
datapath.dataNoRCs.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "No RCs deposited" ls 1, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM DM RCs deposited" ls 5, \
##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 50
unset y2tics
set ytics mirror
set yrange [-25:300]
set key at 0.13,300 font ",16"
unset key
#set key at 0.13,500 font ",16"

set label 1 "b)" at -0.225,300 font ",24" 
#the positioning system is literally garbage...


plot \
datapath.dataNoRCs.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "No RCs" ls 1, \
datapath.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \

unset label 1
################### NO LIGHT#######################

#######################805nm############################### #not actually 805, just called this
set multiplot

set bmargin 0
#bottom margin??

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 10
unset y2tics
set ytics mirror
set yrange [-20:50]
set key at 0.13,50 font ",16"
#key is the legend position. 
set xzeroaxis

set label 1 "Dark state control" at -.1,60 font ", 30"

set label "a)" at -0.225,50 font ",24"
#how this works

#plot <what to plot> <u =?> 1:<scale> axis <which axes> <w=?> <l=?> smooth = smoother csplines=style title "what is it labeled" ls=? 1=:color?
plot \
datapath.dataNoLight.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM no light" ls 1, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "5.0uM" ls 5, \
##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 100
unset y2tics
set ytics mirror
set yrange [-25:300]
#set key at 0.13,50 font ",16"
unset key
set key at 0.18,200 font ",16"

set label 1 "b)" at -0.225,300 font ",24" 
#the positioning system is literally garbage...


plot \
datapath.dataNoLight.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "5.0uM no light" ls 1, \
datapath.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "5.0uM illuminated with light at 805nm" ls 5, \

unset label 1


#########################  phase  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitlePhase
set size @plot_size

set origin 0.15, .55
set ytics 40
unset y2tics
set ytics mirror
set yrange [-180:180]
set key at 0.13,40 font ",16"
set xzeroaxis

set label 1 "" at -.1,60 font ", 30"

set label "" at -0.225,50 font ",24"
#plot data using x:($y * scalefactor) axis which with points/lines title etc
plot \
datapath.data0_5uM.wv680 u 1:($5/2) axis x1y1 with dot title "0.5uM" ls 1, \
datapath.data1_0uM.wv680 u 1:($5/2) axis x1y1 with dot title "1.0uM" ls 2, \
datapath.data2_5uM.wv680 u 1:($5/2) axis x1y1 with dot title "2.5uM" ls 3, \
datapath.data5_0uM.wv680 u 1:($5/2) axis x1y1 with dot title "5.0uM" ls 4, \

unset label

set bmargin 0.05

set ylabel ytitlePhase
set xlabel xtitleAgAgCl offset 0,0.18
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 40
unset y2tics
set ytics mirror
set yrange [-180:180]
#set key at 0.13,3.8
unset key
set key at 0.13,40 font ",16"

set label 1 "" at -0.225,400 font ",24"


plot \
datapath.data0_5uM.wv805 u 1:($5/2) axis x1y1 with dot title "0.5uM" ls 1, \
datapath.data1_0uM.wv805 u 1:($5/2) axis x1y1 with dot title "1.0uM" ls 2, \
datapath.data2_5uM.wv805 u 1:($5/2) axis x1y1 with dot title "2.5uM" ls 3, \
datapath.data5_0uM.wv805 u 1:($5/2) axis x1y1 with dot title "5.0uM" ls 4, \


unset label 1
unset key
#########################  phase  #####################

#set multiplot layout 2,1	#2 high, one wide
set multiplot

set bmargin 0
#set xrange [-0.4:0.4]
#set xtics 0.1

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitlePhase
set size @plot_size

set origin 0.15, .55
set ytics 40
unset y2tics
set ytics mirror
set yrange [-180:180]
set key at 0.13,16 font ",16"
set xzeroaxis

set label 1 "" at -.1,60 font ", 30"

set label "" at -0.225,50 font ",24"
#plot data using x:($y * scalefactor) axis which with points/lines title etc

plot \
datapath.data0_5uM.wv865 u 1:($5/2) axis x1y1 with dot title "0.5uM" ls 1, \
datapath.data1_0uM.wv865 u 1:($5/2) axis x1y1 with dot title "1.0uM" ls 2, \
datapath.data2_5uM.wv865 u 1:($5/2) axis x1y1 with dot title "2.5uM" ls 3, \
datapath.data5_0uM.wv865 u 1:($5/2) axis x1y1 with dot title "5.0uM" ls 4, \

unset label

################################# EFFECT OF MCH #######################
#######################805nm###############################
set multiplot

set bmargin 0
#bottom margin??

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 50
unset y2tics
set ytics mirror
set yrange [-60:150]
set key at 0.30,150 font ",16"
#key is the legend position. 
set xzeroaxis

set label 1 "" at -.1,60 font ", 30"

set label "a)" at -0.225,150 font ",24"
#how this works

#plot <what to plot> <u =?> 1:<scale> axis <which axes> <w=?> <l=?> smooth = smoother csplines=style title "what is it labeled" ls=? 1=:color?
plot \
datapath.data5_0uM24.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "24 hours of 5.0uM RCs and 100uM MCH for 1 hour" ls 1, \
datapath.data5_0uM24_noMCH.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "24 hours of 5.0uM RCs and no MCH" ls 5, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH" ls 2, \

##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 200
unset y2tics
set ytics mirror
set yrange [-25:800]
#set key at 500.13,50 font ",16"
unset key
#set key at 500,400 font ",16"

set label 1 "b)" at -0.225,800 font ",24" 
#the positioning system is literally garbage...


plot \
datapath.data5_0uM24.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "24 hours of 5.0uM RCs and 100uM MCH for 1 hour" ls 1, \
datapath.data5_0uM24_noMCH.wv805 u 1:($3*1e10*1) axis x1y1 w l smooth csplines title "24 hours of 5.0uM RCs and no MCH" ls 5, \
datapath.data5_0uM.wv805 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH" ls 2, \
#wierd bug - I had to write it as 1e10 so in the raw datafile i shrank everything by 10x.
#
unset label 1

unset label
################################# effect of light #######################

set multiplot

set bmargin 0
#bottom margin??

set xlabel ""
set format x ""
# set xlabel xtitleAgAgCl
# set format x "%0.1f"
set format y "%0.0f"
set ylabel ytitleCuruA
set size @plot_size

set origin 0.15, .55
set ytics 50
unset y2tics
set ytics mirror
set yrange [-60:150]
set key at 0.30,150 font ",16"
#key is the legend position. 
set xzeroaxis

set label 1 "" at -.1,60 font ", 30"

set label "a)" at -0.225,150 font ",24"
#how this works

#plot <what to plot> <u =?> 1:<scale> axis <which axes> <w=?> <l=?> smooth = smoother csplines=style title "what is it labeled" ls=? 1=:color?
plot \
datapath.data5_0uM.wv865 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH 865nm" ls 1, \
datapath.data5_0uM.wv805 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH 805nm" ls 5, \
datapath.data5_0uM.wv680 u 1:($7*1e6) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH 685nm" ls 2, \

##########Compare it against the data for normal conditions
unset label

set bmargin 0.05

set ylabel ytitleCurnA
set xlabel xtitleAgAgCl
set format x "%0.2f"
set format y "%0.0f"
set size @plot_size
set origin 0.15, 0.125
set ytics 200
unset y2tics
set ytics mirror
set yrange [-25:400]
#set key at 500.13,50 font ",16"
unset key
#set key at 500,400 font ",16"

set label 1 "b)" at -0.225,400 font ",24" 
#the positioning system is literally garbage...


plot \
datapath.data5_0uM.wv865 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH 865nm" ls 1, \
datapath.data5_0uM.wv805 u 1:($3*1e9*1) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH 805nm" ls 5, \
datapath.data5_0uM.wv680 u 1:($3*1e9) axis x1y1 w l smooth csplines title "1 hour of 5.0uM RCs and 24 hours of MCH 680nm" ls 2, \
#wierd bug - I had to write it as 1e10 so in the raw datafile i shrank everything by 10x.
#
unset label 1

unset label


#cleanup


unset multiplot

quit

