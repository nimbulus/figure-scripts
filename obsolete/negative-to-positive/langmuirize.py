#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 27 13:36:00 2020

@author: randon
"""

import pandas as pd
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import numpy as np
b = pd.read_csv("langmuir-0.15V.csv",'\t')

def langmuir(pa,keq,thetamax):
    #theta/thetamax = keq*pa/(1+keq*pa)
    #assume that 2.5 is ~thetamax
    return (keq*pa/(1+keq*pa))*thetamax
popt, pcov = curve_fit(langmuir, b['[Rcs]'], b['805'])

plt.plot(b['[Rcs]'],b['805'],'.')
plt.plot(b['[Rcs]'], langmuir(b['[Rcs]'], *popt), 'g--')


out = np.zeros(shape=(50,2))
for i in range(50):
    out[i][0] = i/10
    out[i][1]= langmuir(i/10, *popt)
    
np.savetxt("fitted-langmuir0.15V.csv", out, delimiter="\t")
plt.plot(out.transpose()[0],out.transpose()[1])
print("keq, thetamax",popt)
print(pcov)