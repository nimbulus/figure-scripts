## About splitscan.py 

1. This script will take any files with *.csv* in the name, and search for peaks in the column 'WE(1).Potential (V)'. If there are multiple columns named 'WE(1).Potential (V)', it will assume the first is 680, the second is 805, and the third is 865. Since the files from the CSV only have 1 wavelength, and I only used the 805nm ones, they are named "680nm" when they are actually 805. This isn't a problem for the actual data because the conversion factor has been fixed so even though it names the outputs as "680nm", its treating them as if it were 805nm data (5000uV/10v 100uA/V).

2. The script might sometimes bug out...
