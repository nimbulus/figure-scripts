#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 17:32:53 2020

@author: randon
"""
import pandas as pd
from scipy.signal import savgol_filter
from scipy import signal 
from matplotlib import pyplot as plt
import numpy as np
from statistics import mean, stdev 
from itertools import chain, zip_longest
#from numba import jit
#plt.rcParams.update({'font.size': 32})
import os
import csv
#plt.rcParams["figure.figsize"] = (16,12)
#path = "/home/randon/Documents/homework/2019/bizzott/DMs Langmuirian/"

#@jit 

def average(ar):
    if len(ar)>0:
        return sum(ar)/len(ar)
    else:
        return -1
def extractScan(column,direction,boxsize=3,slope=0.01):
    retscan = []
        #returns indices of the scan going in a direction
        #later, one would then do pot = [scan["WE(1).Potential (V)"][i] for i in indices]
    column = np.array(column)
    if direction=="up":
        for i in range(len(column)-boxsize):
            if i>boxsize:
                #print(column[i-boxsize:i+boxsize])
                if mean(column[i-boxsize:i+boxsize])< mean(column[i-boxsize+1:i+boxsize+1]):
                    if abs(mean(column[i-boxsize:i+boxsize])-mean(column[i-boxsize+1:i+boxsize+1]) )<slope:
                #print(i)
                        retscan.append(i)
    else:
        for i in range(len(column)-boxsize):
            if i>boxsize:
                if mean(column[i-boxsize:i+boxsize])>mean(column[i-boxsize+1:i+boxsize+1]):
                    if abs(mean(column[i-boxsize:i+boxsize])-mean(column[i-boxsize+1:i+boxsize+1]) )<slope:
                #print(i)
                        retscan.append(i)
    return retscan
def interleave(a,b):
    return [x for x in chain.from_iterable(zip_longest(a, b)) if x is not None]

def findFullScans(column,direction,name):
    column = np.array(column)
    toppeaks,_a = signal.find_peaks(column,distance=1,prominence=0.05)
    bottompeaks,_d = signal.find_peaks(-1*column,distance=1,prominence=0.05)
    plt.plot(np.arange(len(column)),column)
    plt.plot(bottompeaks,np.ones_like( bottompeaks )*-0.1, 'x')
    plt.plot(toppeaks,np.ones_like( toppeaks )*0.3, 'x')
    plt.title(name)
    plt.show()
    print(bottompeaks,toppeaks, toppeaks[0]-bottompeaks[0])
    #print(bottompeaks,toppeaks)
    #which comes first?
    if toppeaks[0] - bottompeaks[0] > 0:
        #bottompeaks came first
        ups=0 #even pairs (0,1 2,3 4,5)
        indexes = interleave(bottompeaks,toppeaks)
    else:
        ups=1 #odd pairs are ups (1,2 3,4 5,6)
        indexes = interleave(toppeaks,bottompeaks)
    upa = []
    downs = []
    print("indices",indexes)
    for i in range(len(indexes)-1):
        #print(indexes)
        la = list(np.arange(indexes[i],+indexes[i+1]) )
        #print(la[0],la[-1])
        if i%2==ups:#i am an upgoer!
            print("u",i,indexes[i],indexes[i+1])
            upa.append(la)
        else:
            #print("d",i,indexes[i],indexes[i+1])
            downs.append(la)
    if direction=="up":
        return upa
    if direction=="down":
        return downs
def smoother(y,filtersize=5):
    yhat = savgol_filter(y, filtersize, 3) 
    return yhat
def rolling_window(a, window):
    #https://rigtorp.se/2011/01/01/rolling-statistics-numpy.html
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
def stdevs(y,boxsize=11): #returns the stdev within a size 51 box of y
    return np.ones_like(y)
    #return np.std(rolling_window(y, boxsize), 1)
    
def smooth_box(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

mypath = os.getcwd()
files = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
dfs = {}

for file in files:
    if ".csv" in file:
        print(file)
        b = pd.read_csv(file)
        dfs.update({file:b})
    else:
        pass

def process(convfactor,wavkey,wavlen,direction):
    uponly = {}
    for sy in dfs:
        sys = dfs[sy]
        #export files should have the titles
        #Potential	stdev	Photocurrent (nA)	stdev	Phase (°)	stdev	DC Current (µA)	stdev
        try:
            #print(sy)
            indices = findFullScans(sys["WE(1).Potential (V)"+wavkey],direction,sy)
            #print(indices)
            for i in range(len(indices)):
                try:
                    dices = indices[i]
                    try:
                        print("slice",direction,dices[0],dices[len(dices)-1])
                        print(sys["WE(1).Potential (V)"+wavkey][dices[len(dices)-1]])
                    except IndexError:
                        pass
                    #indices = indices[0] #being lazy, only doing the first 
                    #print(indices)
                    potentials = np.array([sys["WE(1).Potential (V)"+wavkey][i] for i in dices])
                    print("maxpot",max(potentials))
                    potstdev = stdevs(potentials)
                    photocurrents = np.array([sys["External(1).External 1 (V)"+wavkey][i] for i in dices])*convfactor
                    photocurrents_sm = smoother(photocurrents)
                    pc_stdev = stdevs(photocurrents)
                    phases = np.array([sys["External(1).External 2 (V)"+wavkey][i] for i in dices])*360/10
                    ph_stdev = stdevs(phases)
                    dcCurr = np.array([sys["WE(1).Current (A)"+wavkey][i] for i in dices])
                    dcCurr_stdev = stdevs(dcCurr)
                    uponly.update({sy+"_"+str(i)+"th_scan":{'Potential':potentials,'potstdev':potstdev,'Photocurrent (nA)':photocurrents, 'pcstdev':pc_stdev,"Phase (°)":phases,"phstdev":ph_stdev,"DC Current (µA)":dcCurr,"dcCurrstdev":dcCurr_stdev} })
                except ValueError as v:
                    #print(v)
                    pass
            #except TypeError:
            #    print("wrong type of file",sy)
        except KeyError:
            #print("missing key for",sy)
            pass

    for item in uponly:
        print(direction,item,"hi")
        my_dict = uponly[item]
        fieldn = my_dict.keys()
        if direction=="up":
            ds = "_negative-to-positive"
        else:
            ds = "_positive-to-negative"
        csv_file = item+ds+"_"+wavlen+'.csv'
        #for key in my_dict.keys():
            #print(len(my_dict[key]))
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile,fieldnames=fieldn)
            writer.writeheader()
            #Detect the bottom of a full scan"
            print("len of potentials:",len(my_dict['Potential'] ))
            for v in range(len(my_dict['Potential'] ) -1):
                try:
                    #print(v)
                    tempdict = {}
                    for key in my_dict.keys():
                        #print(key)
                        tempdict.update({key:list(my_dict[key])[v] } )
                    writer.writerow(tempdict)
                except IndexError:
                    print(v)
                    print(key)
                    print(len(my_dict[key]))
                    print(1/0)

 # for 680 only, 5e-7 for others

for i in ["",".1",".2"]:
    if i == "":
        convfactor = 5e-7
        print("WARNING: assuming '680' is actually 805!")
        wl = "680"
    if i == ".1":
        wl = "805"
        convfactor = 5e-7
    if i ==".2":
        wl = "865"
        convfactor = 5e-7
    process(convfactor,i,wl,"up")
    process(convfactor,i,wl,"down")
