reset
#!/usr/bin/gnuplot -persist

  # Input file contains tab-separated fields
#     set datafile separator "\t"



     # Input file contains tab-separated values fields
     set datafile separator ","


fileout = "dms-dc.ps"	#does not use datapath

# // columns arranged like:
# Potential,potstdev,Photocurrent (nA),pcstdev,Phase (°),phstdev,DC Current (µA),dcCurrstdev
# 0.5 uA/V for 805 & 865
# 0.05 uA/V for 680
data_1uMDMs_100uMMCH = "Chrono_865nm_1uM_DM_RCs_100uM_MCH_(fresh)_24_hours.csv"
data_2_5uMDMs_100uMMCH = "Chrono_865nm_2.5uM_DM_RCs_100uM_MCH_(fresh)_24_hours.csv" #_805.csv"
data_5uMDMs24hrs100uMMCH1hr =  "Chrono_865nm_5uM_DM_RCs_24hrs_100uM_MCH_(fresh)_1hours.csv" #sharper peaks expected
data_5uMDMs24hrs0uMMCH = "Chrono_865nm_5uM_DM_RCs_100uM_MCH_(fresh)_24_hours.csv" #_805.csv"
data_nomch5uMDMs2 =  "nomch5um-2.csv" #_805.csv"
data_nomch5uMDs3 =  "nomch5um-3.csv" #_805.csv"
data_nomch5uMDMs =  "nomch5um.csv" #_805.csv"
data_nomch5um2 = "nomch5um2.csv"
wv680 = "_680.csv"
wv805 = "_805.csv"
wv865 = "_865.csv"


#805cf = 2.636 #correction factor for the power of 805nm LED
#76.89 = 1 #ditto for 680nm LED



datapath = "dcmethods2/csv/"
#@datapathd = "positive-to-negative/"
ttitle = "t (sec)"
xtitle = "E (V)"
xtitleAgAgCl = "E (V vs Ag/AgCl)"
xtitleSCE = "E vs SCE / V"
xtitleNHE = "E vs NHE / V"
ytitleCap = "C_{avg} ({/Symbol m}F cm^{-2})"
ytitleCurnA = "i_{ph} (nA)"
ytitleCuruA = "i ({/Symbol m}A)"
ytitleDeltCuruA = "{/Symbol D} i_{ph} (nA)"
ytitlePhase = "Phase (deg)"
ytitleCurmA = "i (mA)"
ytitleCurPMT = "I_{PMT} ({/Symbol m}A)"
ytitleCurPMTRMS = "I_{PMT}^{RMS} / {/Symbol m}A"
ytitleCurPMTRMSnorm = "I_{PMT}^{RMS} / Fl Int^{RMS}"
ytitlelnCur = "ln(i ({/Symbol m}A))"
ytitlelnCurPMT = "ln(I_{PMT} ({/Symbol m}A))"
ytitle_C = "C_{avg} ({/Symbol m}F)"
ytitleFlIntnorm = "Fl Int^{RMS} / {/Symbol m}A"

set macro
legend_style = "center font \"Times-Roman, bold, 10\" textcolor rgb \"black\" "

plot_size = "0.8, 0.38"

########################################
#These settings can probably stay the same
########################################
##Area = 0.26
set style line 9 lt 1 lw 2 pt 3 lc rgb "black"
set style line 1 lt 1 lw 2 pt 5  lc rgb "red"
set style line 2 lt 1 lw 2 pt 7  lc rgb "blue"
set style line 3 lt 1 lw 2 pt 9  lc rgb "green"
set style line 4 lt 4 lw 2 pt 11  lc rgb "black"
set style line 5 lt 5 lw 2 pt 13  lc rgb "magenta"
set style line 6 lt 1 lw 2 pt 15  lc rgb "dark-red"
set style line 7 lt 1 lw 2 pt 17  lc rgb "dark-blue"
set style line 8 lt 1 lw 2 pt 19  lc rgb "dark-green"
set style line 11 lt 1 lw 2 pt 5 dt "-" lc rgb "red"
set style line 12 lt 1 lw 2 pt 7 dt "-" lc rgb "blue"
set style line 13 lt 1 lw 2 pt 9 dt "-" lc rgb "green"
set style line 14 lt 4 lw 2 pt 11 dt "-"  lc rgb "black"
set style line 21 lt 1 lw 2 pt 5 dt ".-" lc rgb "red"
set style line 22 lt 1 lw 2 pt 7 dt ".-" lc rgb "blue"
set style line 23 lt 1 lw 2 pt 9 dt ".-" lc rgb "green"
set style line 24 lt 4 lw 2 pt 11 dt ".-" lc rgb "black"
set style line 19 lt 1 lw 3 pt 3 dt "-" lc rgb "black"

##########size for journal 90mm 140mm 190mm
set terminal postscript portrait size 19cm, 15cm color enhanced lw 1 font "Times-Roman,20"

#set terminal postscript color enhanced 11
set output fileout

#set size 0.5,1

set lmargin 0.25
set rmargin 0.05
#set size 0.8, 0.45


#set title ""
unset title

#########################  805 nm  #####################
ymax_pc = 0.3
ymin=-1 

#set multiplot layout 2,1	#2 high, one wide
set multiplot 
set bmargin 0
#set xrange [-0.4:0.4]
set xtics 5 font ",14"

#set xlabel ""
#set format x ""
#set xlabel ttitle offset 0,0.8
set format x "%0.1f"
set format y "%0.1f"
set ylabel ytitleDeltCuruA offset 2,0
set size @plot_size

set origin 0.15, .55
#set ytics ymax_pc/10.0
unset y2tics
set ytics mirror
set xzeroaxis
##auto-picks the extremae of
stats datapath.data_5uMDMs24hrs100uMMCH1hr u 1:($2*1e9) nooutput
set xrange [STATS_min_x:25]
set yrange [0:STATS_max_y/2]
#set xtics (STATS_max_x-STATS_min_x)*0.25
set key at (25),STATS_max_y/2 font ",14"


#light on and off rectangles
set style rect fc lt -1 fs solid 0.15 noborder
set object 1 rect from 0.005, graph 0 to 2.3, graph 1 back
set object 2 rect from 5.6, graph 0 to 8.2, graph 1 back
set object 3 rect from 11.2, graph 0 to 13.5, graph 1 back
set object 4 rect from 15.8, graph 0 to 18, graph 1 back
set object 5 rect from 20.7, graph 0 to 23.5, graph 1 back

set label 1 "805 nm" at -.1,STATS_max_y/2 + 10 font ", 30"

set label "a)" at -2,STATS_max_y/2 + 10 font ",24"

plot datapath.data_5uMDMs24hrs100uMMCH1hr u 1:($2*1e9 - STATS_min_y) axis x1y1 w l title "5uM DMs for 24 hours and 1 hour of 100 uM of MCH" ls 1


unset label
unset yrange
unset xrange

set bmargin 0.05
set ylabel ytitleDeltCuruA offset 2,0
set xlabel ttitle offset 0,0.6
set format x "%0.1f"
set format y "%0.1f"
set size @plot_size
set origin 0.15, 0.125
unset y2tics
set ytics mirror
#set yrange [ymin:ymax_pc]
#set key at 0.13,3.8
unset key
stats datapath.data_nomch5um2 u 3:($4*1e9) nooutput

set label 1 "b)" at -2,STATS_max_y/4 + 10 font ",24"
##auto-picks the extremae of

set xrange [STATS_min_x:25]
set yrange [0:STATS_max_y/4]
set key at (25),STATS_max_y/4 font ",14"
#set xtics (STATS_max_x-STATS_min_x)*0.25
#light on and off rectangles
set style rect fc lt -1 fs solid 0.15 noborder
set object 1 rect from 0.4, graph 0 to 8, graph 1 back
set object 2 rect from 14.5, graph 0 to 24, graph 1 back

unset object 3
unset object 4
unset object 5



plot \
datapath.data_nomch5um2 u 3:($4*1e9 - STATS_min_y) axis x1y1 w l title "5uM DMs for 24 hours and no MCH" ls 2, \

unset label 1
unset key
#cleanu
unset multiplot

quit

